###########################################
# Author: David Rodenkirchen              #
# E-Mail: davidr.develop@googlemail.com   #
# Licence: GPL-3.0-or-later               #
###########################################

VERSION = "0.1.0"


def init_globals():
    global global_current_map_id
    global_current_map_id = "0"
