###########################################
# Author: David Rodenkirchen              #
# E-Mail: davidr.develop@googlemail.com   #
# Licence: GPL-3.0-or-later               #
###########################################
import requests
import popups
import configparser
import os.path
from sys import exit as exit_gw2rpc

"""
This component handles the connection and all requests between the PyQt Frontend and the FLASK Backend
"""


class Singleton:
    """
    This class is a blueprint to implement a singleton-like behavior for Python classes.
    To use this on any class, decorate your class with `@Singleton` and "instantiate" that class
    with `my_class = MyClass.instance()`

    See also
    ----------
    backend_com.BackendCom
    """

    def __init__(self, decorated):
        self._decorated = decorated

    def instance(self):
        """
        This method needs to be invoked to get the singleton instance of the decorated class.

        Returns
        -------
        Singleton instance of the decorated class
        """
        try:
            return self._instance
        except AttributeError:
            self._instance = self._decorated()
            return self._instance

    def __call__(self):
        """
        This method overwrites the default __call__ of Python Classes. It raises a TypeError if someone
        accidentally tries to use the singleton class like a regular class (tries to instantiate a new object)

        Raises
        -------
        TypeError
        """
        raise TypeError('Singletons must be accessed through `instance()`.')

    def __instancecheck__(self, inst):
        return isinstance(inst, self._decorated)


@Singleton
class BackendCom:
    """
    BackendCom is a singleton class that provides its users with the ability to call the GW2RPC backend.
    It wraps the requests and responses in dedicated class methods.

    Attributes
    ----------
    config : ConfigParser.Section
        Holds the currently active configuration in a dictionary-like structure.
        Keys: IP, PORT, API_ROOT_ENDPOINT
    auth_key : str
        The GW2RPC authentication key that was read from file.
        Is an empty string if the file `AUTH_KEY.gw2rpc` could not be found.
    BackendCom.GW2RPC_MODE : str
        This is either "PROD" or "DEBUG". It is an easy way to access the mode GW2RPC is currently in
        without the need to get a full BackendCom singleton instance. If you need it you can use
        `from backend_com import GW2RPC_MODE`.
    """
    GW2RPC_MODE = "PROD"

    def __init__(self):
        self.config = self._get_config()
        self.auth_key = self._get_auth_key()

    @staticmethod
    def _get_auth_key():
        """
        Reads a file named `AUTH_KEY.gw2rpc` from the applications base directory.
        If not found or empty, a popup is shown, notifying the user about the missing/empty key.

        Returns
        -------
        str:
            Either the authentication key or an empty string
        """
        try:
            with open("./AUTH_KEY.gw2rpc", "r") as auth_key_file:
                auth_key = auth_key_file.readline().rstrip()
                if auth_key == "":
                    popups.pre_start_info_window("info", "auth key empty", "", predefined="AUTHKEY_EMPTY")
                    return ""
                return auth_key
        except FileNotFoundError:
            popups.pre_start_info_window("info", "auth key missing", "", predefined="AUTHKEY_MISSING")
            return ""

    @staticmethod
    def _get_config():
        """
        Tries to initialize a ConfigParser object from a file named `config.gw2rpc`. Notifies user about missing
        config file and exits if config file not found or has missing section(s).

        Returns
        -------
        configparser.ConfigParser.Section
            Either the section "PROD" or "DEBUG", depending on the setting in "GENERAL"
        """
        _config = configparser.ConfigParser()
        if not os.path.isfile("config.gw2rpc"):
            popups.pre_start_info_window("error", "server config not found", "", predefined="CONFIG_NOT_FOUND")
            exit_gw2rpc("config file could not be found")
        _config.read("config.gw2rpc")
        if "PROD" not in _config or "DEBUG" not in _config or "GENERAL" not in _config:
            exit_gw2rpc("config file is incomplete")
            popups.pre_start_info_window("error", "server config incomplete", "", predefined="CONFIG_INCOMPLETE")
        BackendCom.GW2RPC_MODE = _config["GENERAL"]["MODE"]
        return _config[BackendCom.GW2RPC_MODE]

    def get_data_from_endpoint(self, endpoint):
        """
        Uses the request module to send a GET request to the specified GW2RPC Backend. Adds the users
        authentication key to the request header.

        Parameters
        ----------
        endpoint : str
            API endpoint the request will be sent to

        Returns
        -------
        response : Union[dict, bool]
            Returns the `response` part of the original response JSON if there were no issues with the request.
            Returns `False` if the was an ConnectionError, the responses status code was not `OK` or the
            HTTP requests status code was not `200`.
        """
        try:
            r = requests.get(f"http://{self.config['IP']}:{self.config['PORT']}/{self.config['API_ROOT_ENDPOINT']}/"
                             f"{endpoint}", headers={"Auth-Key": self.auth_key})
        except requests.exceptions.ConnectionError:
            return False
        if r.status_code == 200:
            r = r.json()
            if r["status_code"] == "OK":
                return r["response"]
            return False
        return False

    def send_payload_to_endpoint(self, endpoint, payload):
        """
        Uses the request module to send a POST request to the specified GW2RPC Backend. Adds the users
        authentication key to the request header.

        Parameters
        ----------
        endpoint : str
            API endpoint the request will be sent to.
        payload : dict
            Holds the endpoint specific data in a dictionary.

        Returns
        -------
        response : Union[dict, bool]
            Returns the `response` part of the original response JSON if there were no issues with the request.
            Returns `False` if the was an ConnectionError, the responses status code was not `OK` or the
            HTTP requests status code was not `200`.
        """
        try:
            r = requests.post(f"http://{self.config['IP']}:{self.config['PORT']}/{self.config['API_ROOT_ENDPOINT']}/"
                              f"{endpoint}", headers={"Auth-Key": self.auth_key}, data=payload)
        except requests.exceptions.ConnectionError:
            return False
        if r.status_code == 200:
            r = r.json()
            if r["status_code"] == "OK":
                return r["response"]
            return False
        return False

    # request to update a character
    def update_character(self, char_id, new_character_values, activate):
        """
        Updates the character with ID `char_id` at the backend.

        Parameters
        ----------
        char_id : Union[int, str]
            Character ID of the character that will be updated.
        new_character_values : dict
            Holds the new character data.
        activate : bool
            adds `1` to `new_character_values` if value is True otherwise adds `0` to `new_character_values`

        Returns
        -------
        response : bool
            Returns `True` if update was successful, otherwise `False`.
        """
        new_character_values.update(char_id)
        if activate:
            new_character_values["active"] = 1
        else:
            new_character_values["active"] = 0
        if len(new_character_values["forename"]) < 1:
            return False
        return bool(self.send_payload_to_endpoint("characters/update", new_character_values))

    def delete_character(self, char_id):
        """
        Deletes the character with ID `char_id` at the backend.

        Parameters
        ----------
        char_id : Union[int, str]
            Character ID of the character that will be deleted.

        Returns
        -------
        response : bool
            Returns `True` if deletion was successful, otherwise `False`.
        """
        return bool(self.send_payload_to_endpoint("characters/delete", {"char_id": char_id}))

    def add_character(self, new_character_forename):
        """
        Adds a character with new forename `new_character_forename` at the backend.

        Parameters
        ----------
        new_character_forename : str
            Character forename of the character that will be added.

        Returns
        -------
        response : bool
            Returns `True` if adding the character was successful, otherwise `False`.
        """
        return bool(self.send_payload_to_endpoint("characters/add", {"forename": new_character_forename}))

    def send_heartbeat(self):
        """
        Sends an empty GET request to the backend so the backend acknowledges that the client is online.

        Returns
        -------
        None
        """
        self.get_data_from_endpoint("accounts/heartbeat")  # doesnt matter if ok or not

    def get_all_online_players(self):
        """
        Requests a list of all players who are online.

        Returns
        -------
        response : list of lists [UserName, CharacterName]
            Returns list of all players who are deemed "online" by the backend.
            Returns empty list in case anything went wrong.
        """
        list_of_online_players = self.get_data_from_endpoint("public/online")
        if not list_of_online_players:
            return []
        return list_of_online_players

    def get_all_online_players_on_map(self, map_id):
        """
        Requests a list of all players who are online and on the map with MapID `map_id`.

        Parameters
        ----------
        map_id : dict {"map_id": MapID}
            The MapID as an Union[int|str]

        Returns
        -------
        response : list of lists [UserName, CharacterName]
            Returns list of all players who are deemed "online" by the backend.
            Returns empty list in case anything went wrong.

        See also
        --------
        get_all_online_players
        """
        list_of_online_players_on_map = self.send_payload_to_endpoint("public/online", {"map_id": map_id})
        if not list_of_online_players_on_map:
            return []
        return list_of_online_players_on_map

    def get_active_character_by_account_name(self, account_name):
        """
        Requests a full character data-set for the active character for the requested account.

        Parameters
        ----------
        account_name : str
            account name (synonym to user name) of the user from which the active character is requested.

        Returns
        -------
        response : list with single dict {{'accessories': str, 'active': str, 'age': str, 'bio': str, 'body_type': str,
        'char_id': int, 'current_status': str, 'eyes': str, 'forename': str, 'hair_fur': str, 'middlename': str,
        'notable_characteristics': str, 'posture': str, 'race': str, 'racial_features': str, 'surname': str,
        'title': str, 'voice': str}}
            Returns a list with a single dictionary containing a full character dataset. (Excluding the active status
            since this is implicitly always "yes")
            Returns `False` if the requested user has no active characters
        """
        active_character = self.send_payload_to_endpoint("public/getactivecharacter", {"account_name": account_name})
        if not active_character:
            return False
        return active_character

    def get_user_status(self, account_name):
        """
        Requests a full dataset of a user.

        Parameters
        ----------
        account_name : str
            account name (synonym to user name) of the user from which the status data is requested.

        Returns
        -------
        response : dict {'account_name': str, 'lore_strictness': str, 'ooc_status': str, 'pref_lang': str,
        'rp_exp': str, 'rp_intensity': str, 'rp_type_pref': str}
            Returns dict with data about the user.
            Returns False is anything goes wrong.
        """
        user_status = self.send_payload_to_endpoint("public/getuserstatus", {"account_name": account_name})
        if not user_status:
            return False
        return user_status

    def set_current_map(self, current_map_id):
        """
        Sets the current map for the user.

        Parameters
        ----------
        current_map_id : Union[str|int]
            map id the user is currently on.

        Returns
        -------
        response : bool
            Returns True if setting the current map was successful, False if there was any kind of error.
        """
        return bool(self.send_payload_to_endpoint("accounts/updatecurrentmap", {"current_map_id": current_map_id}))

    def get_own_character_bio(self, char_id):
        """
        Gets the biography text for the requested character

        Parameters
        ----------
        char_id : Union[str|int]
            character ID of the character from which the biography text is requested.

        Returns
        -------
        response : str
            Biography text for the requested character.
        """
        return self.send_payload_to_endpoint("characters/getbio", {"char_id": char_id})

    def set_own_character_bio(self, char_id, new_bio):
        """
        Sets the biography text for the requested character

        Parameters
        ----------
        char_id : Union[str|int]
            character ID of the character for which the biography text will be set.
        new_bio : str
            Biography text that will be set.

        Returns
        -------
        response : bool
            Returns True if setting the biography was successful, False if there was any kind of error.
        """
        return bool(self.send_payload_to_endpoint("characters/setbio", {"char_id": char_id, "new_bio": new_bio}))

    def create_event(self, new_event_payload):
        """
        Sends data for a new event to the backend and requests it's creation.

        Parameters
        ----------
        new_event_payload : dict { "name":str, "location": str, "start": str,
        "end": str, "publicity": str, "server": str, "description": str }
            Data for the event that will be created

        Returns
        -------
        response : Union [bool|str]
            Returns a string containing the new event's code if creation is successful, otherwise False
        """

        response = self.send_payload_to_endpoint("events/create", new_event_payload)
        if not response:
            return False
        return response

    def join_event(self, event_code):
        """
        Sends an event code to the backend with the request to join that event.

        Parameters
        ----------
        event_code : str
            Six characters long string containing the unique event code

        Returns
        -------
        response : bool
            Returns True if joining the event was successful, False if not.
        """
        return bool(self.send_payload_to_endpoint("events/join", {"event_code": event_code}))

    def get_joined_events(self):
        """
        Gets all events (short info) in which the user has joined.

        Returns
        -------
        response : [{"name": str, "host": str, "start": str, "end": str, "code": str}, n]
            Returns list of dicts, each dict representing a event (short info), list can be empty.

        """
        return self.get_data_from_endpoint("events/getjoined")

    def get_single_event_data(self, event_code):
        """
        Gets all the data about a single event with the specified code.

        Returns
        -------
        response : {"name": str, "location": str, "public": int, "server": str, "code": str, "description": str,
                    "latest_news": str, "start": str, "end": str, "host": str}
            Returns single dict with all the data. Returns False if there is no such event.
        """
        return self.send_payload_to_endpoint("events/getsingleevent", {"event_code": event_code})

    def get_all_public_events(self):
        """
        Gets all publicly listed events.

        Returns
        -------
        response : [{"name": str, "code": str, "start": str, "end": str}, n]
            Returns a list with a dict for each event. List can be empty
        """
        all_public_events = self.get_data_from_endpoint("events/getpublic")
        return all_public_events if all_public_events else []

    def delete_event(self, event_code):
        """
        Deletes a single event.

        Parameters
        ----------
        event_code : str
            Six characters long string containing the unique event code

        Returns
        -------
        response : bool
            Returns if deletion was successful or not.
        """
        return bool(self.send_payload_to_endpoint("events/delete", {"event_code": event_code}))

    def update_event(self, payload):
        """
        Updates a single event.

        Parameters
        ----------
        payload : dict
            Full event payload

        Returns
        -------
        response : bool
            Returns if update was successful or not.

        See also
        --------
        get_single_event_data
        """
        return bool(self.send_payload_to_endpoint("events/update", payload))

    def leave_event(self, event_code):
        """
        Leaves a single event.

        Parameters
        ----------
        event_code : str
            Six characters long string containing the unique event code

        Returns
        -------
        response : bool
            Returns if leaving was successful or not.
        """
        return bool(self.send_payload_to_endpoint("events/leave", {"event_code": event_code}))

    def get_latest_version(self) -> str:
        """
        Gets the latest version. Format: X.Y.Z
        Return 0.0.0 on error

        Returns
        -------
        latest_version : str
            See method description.
        """
        try:
            return str(self.get_data_from_endpoint("meta/version/latest")["latest_version"])
        except KeyError:
            return "0.0.0"
