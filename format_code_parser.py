###########################################
# Author: David Rodenkirchen              #
# E-Mail: davidr.develop@googlemail.com   #
# Licence: GPL-3.0-or-later               #
###########################################
import re


# replaces format codes with HTML equivalent
# for more info see biography_editor.py -> show_formatting_help
def parse_formatted_code(formatted_code):
    formatted_code = str(formatted_code)
    formatted_code = re.sub('<[^>]*>', '', formatted_code)  # removes all HTML tags
    formatted_code = formatted_code.replace("[h1]", "<h1>").replace("[/h1]", "</h1>")
    formatted_code = formatted_code.replace("[h2]", "<h2>").replace("[/h2]", "</h2>")
    formatted_code = formatted_code.replace("[h3]", "<h3>").replace("[/h3]", "</h3>")
    formatted_code = formatted_code.replace("[h4]", "<h4>").replace("[/h4]", "</h4>")
    formatted_code = formatted_code.replace("[h5]", "<h5>").replace("[/h5]", "</h5>")
    formatted_code = formatted_code.replace("[h6]", "<h6>").replace("[/h6]", "</h6>")
    formatted_code = formatted_code.replace("[i]", "<i>").replace("[/i]", "</i>")
    formatted_code = formatted_code.replace("[b]", "<b>").replace("[/b]", "</b>")
    formatted_code = formatted_code.replace("[sub]", "<sub>").replace("[/sub]", "</sub>")
    formatted_code = formatted_code.replace("[sup]", "<sup>").replace("[/sup]", "</sup>")
    formatted_code = formatted_code.replace("[strike]", "<s>").replace("[/strike]", "</s>")
    formatted_code = formatted_code.replace("[u]", "<u>").replace("[/u]", "</u>")
    formatted_code = formatted_code.replace("[c]", "<center>").replace("[/c]", "</center>")
    formatted_code = formatted_code.replace("[small]", "<small>").replace("[/small]", "</small>")
    formatted_code = formatted_code.replace("\n", "<br>")
    # colored text
    while re.search('\[color:#?\w+\]', formatted_code) is not None:
        _color_tag = re.search('\[color:#?\w+\]', formatted_code)
        _color = _color_tag.group().split(":")[-1][:-1]
        formatted_code = re.sub('\[color:#?\w+\]', f'<span style="color:{_color}">', formatted_code, 1)
    formatted_code = formatted_code.replace("[/color]", "</span>")
    # links
    while re.search('\[link:https?:\/\/.*?\]', formatted_code) is not None:
        _link_tag = re.search('\[link:https?:\/\/.*?\]', formatted_code)
        _link = _link_tag.group().split(":", 1)[-1][:-1]
        formatted_code = re.sub('\[link:https?:\/\/.*?\]', f'<a href="{_link}" style="color:#CCBA1D">', formatted_code, 1)
    formatted_code = formatted_code.replace("[/link]", "</a>")
    # dividers
    while re.search('\[divider:\d\]', formatted_code) is not None:
        _divider_tag = re.search('\[divider:\d\]', formatted_code)
        _divider = _divider_tag.group().split(":")[-1][:-1]
        formatted_code = re.sub('\[divider:\d\]', f'<img src=\"./views/img/divider{_divider}.png\">', formatted_code, 1)
    return formatted_code
