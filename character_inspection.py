###########################################
# Author: David Rodenkirchen              #
# E-Mail: davidr.develop@googlemail.com   #
# Licence: GPL-3.0-or-later               #
###########################################
from PyQt5.QtCore import QRect
from PyQt5.QtWidgets import QGroupBox, QLabel, QTextEdit, QPushButton, QTextBrowser
import link_opener
import format_code_parser


class CharacterInspector(QGroupBox):
    def __init__(self):
        super().__init__()

        self.setObjectName("CharacterInspector")
        self.text_boxes = {}
        self.bio = ""
        self.setFlat(True)

        def generate_label(parent_obj, size_qrect, text):
            label = QLabel(parent_obj)
            label.setGeometry(size_qrect)
            label.setText(text)
            return label

        def generate_textbox(parent_obj, x, y):
            le = QTextEdit(parent_obj)
            le.setGeometry(QRect(x, y, 290, 45))
            le.setText("")
            le.setEnabled(True)
            return le

        self.setGeometry(QRect(0, 0, 960, 360))
        # full name
        self.full_name_label = generate_label(self, QRect(15, 4, 900, 20), "")
        # current status
        self.status_label = generate_label(self, QRect(15, 21, 900, 20), "<b>current status:</b> <i>none</i>")

        # race
        generate_label(self, QRect(15, 55, 160, 30), "race")
        self.text_boxes["race"] = generate_textbox(self, 170, 55)
        # eyes
        generate_label(self, QRect(15, 110, 160, 30), "eyes")
        self.text_boxes["eyes"] = generate_textbox(self, 170, 110)
        # hair/fur
        generate_label(self, QRect(15, 165, 160, 30), "hair / fur")
        self.text_boxes["hair_fur"] = generate_textbox(self, 170, 165)
        # age
        generate_label(self, QRect(15, 220, 160, 30), "age")
        self.text_boxes["age"] = generate_textbox(self, 170, 220)
        # body type
        generate_label(self, QRect(15, 275, 160, 30), "body type")
        self.text_boxes["body_type"] = generate_textbox(self, 170, 275)
        # voice
        generate_label(self, QRect(480, 55, 160, 30), "voice")
        self.text_boxes["voice"] = generate_textbox(self, 640, 55)
        # accessories
        generate_label(self, QRect(480, 110, 160, 30), "accessories")
        self.text_boxes["accessories"] = generate_textbox(self, 640, 110)
        # notable characteristics
        generate_label(self, QRect(480, 165, 160, 30), "notable characteristics")
        self.text_boxes["notable_characteristics"] = generate_textbox(self, 640, 165)
        # racial features
        generate_label(self, QRect(480, 220, 160, 30), "racial features")
        self.text_boxes["racial_features"] = generate_textbox(self, 640, 220)
        # posture
        generate_label(self, QRect(480, 275, 160, 30), "posture")
        self.text_boxes["posture"] = generate_textbox(self, 640, 275)

        self.view_bio_button = QPushButton("view characters biography", self)
        self.view_bio_button.setGeometry(QRect(10, 325, 450, 25))
        self.view_bio_button.clicked.connect(self.show_bio)

        self.back_button = QPushButton("back", self)
        self.back_button.setGeometry(QRect(500, 325, 450, 25))
        self.back_button.clicked.connect(self.hide)

    def update_view(self, character):
        ci = character
        self.bio = ci["bio"]
        for k in ci.keys():
            if k in self.text_boxes.keys():
                self.text_boxes[k].setText(ci[k])
                self.text_boxes[k].setReadOnly(True)
        self.status_label.setText(f"<b>current status:</b> <i>{ci['current_status']}</i>")
        self.full_name_label.setText(" ".join(filter(None, [ci["title"], ci["forename"], ci["middlename"], ci["surname"]])))

    def show_bio(self):
        show_bio_box = QGroupBox(self)
        show_bio_box.setFlat(True)
        show_bio_box.setGeometry(QRect(0, 0, 960, 360))
        show_bio_box.setObjectName("BiographyPreviewer")

        show_bio_text_edit = QTextBrowser(show_bio_box)
        show_bio_text_edit.setGeometry(QRect(0, 0, 960, 310))
        show_bio_text_edit.setOpenExternalLinks(True)
        show_bio_text_edit.setOpenLinks(False)
        show_bio_text_edit.anchorClicked.connect(lambda link: link_opener.OpenLinkWindow(show_bio_box, link))
        text = format_code_parser.parse_formatted_code(self.bio)
        show_bio_text_edit.setText(text)

        back_button = QPushButton(show_bio_box)
        back_button.setGeometry(QRect(10, 320, 940, 30))
        back_button.setText("back")
        back_button.clicked.connect(lambda: show_bio_box.setParent(None))
        show_bio_box.show()
