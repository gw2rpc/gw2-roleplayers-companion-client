###########################################
# Author: David Rodenkirchen              #
# E-Mail: davidr.develop@googlemail.com   #
# Licence: GPL-3.0-or-later               #
###########################################
from PyQt5.QtCore import QRect
from PyQt5.QtWidgets import QGroupBox, QLabel, QPushButton
from PyQt5.QtGui import QDesktopServices


class OpenLinkWindow(QGroupBox):
    def __init__(self, parent, link):
        super().__init__()
        self.setParent(parent)
        self.link = link
        self.setFlat(True)
        self.setObjectName("OpenLinkWindow")
        self.setGeometry(QRect(355, 100, 250, 150))
        warning_header = QLabel("External Link", self)
        warning_header.setGeometry(QRect(90, 10, 120, 30))
        warning_text = QLabel(self)
        warning_text.setGeometry(QRect(10, 35, 230, 50))
        warning_text.setText(f"This link will take you to:\n{link.host()}")
        _confirm_button = QPushButton("open link", self)
        _confirm_button.setGeometry(QRect(140, 110, 100, 30))
        _confirm_button.clicked.connect(lambda: QDesktopServices.openUrl(link) and self.setParent(None))
        _decline_button = QPushButton("cancel", self)
        _decline_button.setGeometry(QRect(10, 110, 100, 30))
        _decline_button.clicked.connect(lambda: self.setParent(None))
        self.show()
