###########################################
# Author: David Rodenkirchen              #
# E-Mail: davidr.develop@googlemail.com   #
# Licence: GPL-3.0-or-later               #
###########################################
from PyQt5.QtCore import QRect
from PyQt5.QtWidgets import QGroupBox, QTextEdit, QPushButton, QTextBrowser
import link_opener
import backend_com
import format_code_parser


class BiographyEditor(QGroupBox):
    def __init__(self, parent, current_character_id):
        super().__init__()
        self.backend_communicator = backend_com.BackendCom.instance()
        self.setParent(parent)
        self.setGeometry(QRect(0, 0, 960, 360))
        self.setObjectName("BiographyEditor")
        self.setFlat(True)
        self.current_character_id = current_character_id
        self.current_character_bio = self.backend_communicator.get_own_character_bio(self.current_character_id)

        # Editor
        self.editor = QTextEdit(self)
        self.editor.setText(self.current_character_bio if self.current_character_bio else "")
        self.editor.setGeometry(QRect(0, 0, 960, 310))
        self.editor.setAcceptRichText(False)
        self.editor.textChanged.connect(lambda: self.save_button.setEnabled(True))

        # Controls
        self.save_button = QPushButton(self)
        self.save_button.setGeometry(QRect(715, 320, 235, 30))
        self.save_button.setText("save")
        self.save_button.setEnabled(False)
        self.save_button.clicked.connect(lambda: self.save_button.setEnabled(not
                                                                             self.backend_communicator.
                                                                             set_own_character_bio(
                                                                                 self.current_character_id,
                                                                                 self.editor.toPlainText())))

        self.preview_button = QPushButton(self)
        self.preview_button.setGeometry(QRect(480, 320, 235, 30))
        self.preview_button.setText("preview")
        self.preview_button.clicked.connect(self.previewer)

        self.back_button = QPushButton(self)
        self.back_button.setGeometry(QRect(10, 320, 235, 30))
        self.back_button.setText("back")
        self.back_button.clicked.connect(lambda: self.hide())

        self.help_button = QPushButton(self)
        self.help_button.setGeometry(QRect(245, 320, 235, 30))
        self.help_button.setText("formatting help")
        self.help_button.clicked.connect(self.show_formatting_help)

    def previewer(self):
        preview_box = QGroupBox(self)
        preview_box.setObjectName("BiographyPreviewer")
        preview_box.setFlat(True)
        preview_box.setGeometry(QRect(0, 0, 960, 360))

        preview_text_edit = QTextBrowser(preview_box)
        preview_text_edit.setGeometry(QRect(0, 0, 960, 310))
        preview_text_edit.setOpenExternalLinks(True)
        preview_text_edit.setOpenLinks(False)
        preview_text_edit.anchorClicked.connect(lambda link: link_opener.OpenLinkWindow(preview_box, link))
        preview_text_edit.setReadOnly(True)
        text = format_code_parser.parse_formatted_code(self.editor.toPlainText())
        preview_text_edit.setText(text)

        back_button = QPushButton(preview_box)
        back_button.setGeometry(QRect(10, 320, 940, 30))
        back_button.setText("back")
        back_button.clicked.connect(lambda: preview_box.setParent(None))
        preview_box.show()

    def show_formatting_help(self):
        formatting_help_box = QGroupBox(self)
        formatting_help_box.setFlat(True)
        formatting_help_box.setGeometry(QRect(0, 0, 960, 360))

        formatting_text_edit = QTextBrowser(formatting_help_box)
        formatting_text_edit.setOpenExternalLinks(True)
        formatting_text_edit.setGeometry(QRect(0, 0, 960, 310))
        formatting_text_edit.setReadOnly(True)
        text = """
        <h1>[h1]Heading 1[/h1]</h1>
        <h2>[h2]Heading 2[/h2]</h2>
        <h3>[h3]Heading 3[/h3]</h3>
        <h4>[h4]Heading 4[/h4]</h4>
        <h5>[h5]Heading 5[/h5]</h5>
        <h6>[h6]Heading 6[/h6]</h6>
        <br>
        <i>[i]italic[/i]</i><br>
        <b>[b]bold[/b]</b><br>
        <sub>[sub]sub script[/sub]</sub><br>
        <s>[strike]strike trough[/strike]</s><br>
        <a href="#">[link:LINK]link[/link]</a><br>
        <sup>[sup]super script[/sup]</sup><br>
        <center>[c]centered[/c]</center><br>
        <small>[small]small text[/small]</small><br>
        <span style="color:DARKRED">[color:COLOR]colored text[/color]</span> <i>either <a href="https://www.w3schools.com/colors/colors_picker.asp" style="color:#CCBA1D">HEX values</a> or <a href="https://www.w3.org/wiki/CSS/Properties/color/keywords" style="color:#CCBA1D">colorcodes</a></i><br><br>
        [divider:NUMBER]<br>
        1: <img src=\"./views/img/divider1.png\"><br>
        2: <img src=\"./views/img/divider2.png\"><br>
        3: <img src=\"./views/img/divider3.png\"><br>
        4: <img src=\"./views/img/divider4.png\"><br>
        5: <img src=\"./views/img/divider5.png\"><br>
        """
        formatting_text_edit.setHtml(text)

        back_button = QPushButton(formatting_help_box)
        back_button.setGeometry(QRect(10, 320, 940, 30))
        back_button.setText("back")
        back_button.clicked.connect(lambda: formatting_help_box.setParent(None))
        formatting_help_box.show()
