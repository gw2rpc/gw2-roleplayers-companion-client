###########################################
# Author: David Rodenkirchen              #
# E-Mail: davidr.develop@googlemail.com   #
# Licence: GPL-3.0-or-later               #
###########################################
from PyQt5.QtWidgets import QApplication, QWidget, QMessageBox

PREDEFINED_POPUP_MESSAGES = {
    "AUTHKEY_MISSING": "It looks like your auth key is missing.\n"
                       "Make sure the GW2RPC base folder contains a file named \""
                       "AUTH_KEY.gw2rpc\".\nIf you are a alpha tester and don't "
                       "have one yet, visit the GW2RPC Discord server.\n\n"
                       "GW2RPC will now continue running, with limited "
                       "usability.",
    "AUTHKEY_EMPTY": "It looks like your auth key file is empty.\n"
                     "Make sure the AUTH_KEY.gw2rpc file contains a your auth key."
                     "\nIf you are a alpha tester and don't "
                     "have one yet, visit the GW2RPC Discord server.\n\n"
                     "GW2RPC will now continue running, with limited "
                     "usability.",
    "CONFIG_INCOMPLETE": "The configuration file (config.gw2rpc) is malformed or incomplete.\n"
                         "GW2RPC can not continue running before this issue is resolved.",
    "CONFIG_NOT_FOUND": "The configuration file (config.gw2rpc) could not be found.\n"
                        "GW2RPC can not continue running before this issue is resolved."
}


def show_error_popup(title, text):
    err_msg = QMessageBox()
    err_msg.setIcon(QMessageBox.Critical)
    err_msg.setWindowTitle(title)
    err_msg.setText(text)
    err_msg.exec_()


def show_info_popup(title, text):
    err_msg = QMessageBox()
    err_msg.setIcon(QMessageBox.Information)
    err_msg.setWindowTitle(title)
    err_msg.setText(text)
    err_msg.exec_()


def pre_start_info_window(msg_type, title, text, predefined=None):
    if predefined:
        text = PREDEFINED_POPUP_MESSAGES[predefined]
    if msg_type == "error":
        show_error_popup(title, text)
    elif msg_type == "info":
        show_info_popup(title, text)
    else:
        return
