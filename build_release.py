###########################################
# Author: David Rodenkirchen              #
# E-Mail: davidr.develop@googlemail.com   #
# Licence: GPL-3.0-or-later               #
###########################################
import os
import sys
import shutil


def _prepare_and_run_pyinstaller(version):
    with open("version.example.yaml", "r") as template_version_file:
        modified_template_version_file = template_version_file.read()
        modified_template_version_file = modified_template_version_file.replace("0.0.0", version)
    with open("temp_version.yaml", "w") as temp_version_yaml:
        temp_version_yaml.write(modified_template_version_file)
    os.system("create-version-file temp_version.yaml --outfile file_version_info.txt")
    os.remove("temp_version.yaml")
    os.system('pyinstaller -w -i gw2rpc_ico.ico --version-file file_version_info.txt -n "GW2RPC" main.py')
    os.remove("file_version_info.txt")


def _copy_dependencies():
    dist_dir = "./dist/GW2RPC/"
    if not os.path.exists(dist_dir):
        sys.exit("Couldn't find dist directory")
    shutil.copy2("./map_ids.json", dist_dir)  # MapID's
    shutil.copy2("./config.gw2rpc", dist_dir)  # Server Config
    shutil.copy2("./style.qss", dist_dir)  # Stylesheet
    shutil.copytree("./views/img", os.path.join(dist_dir, "views/img"))  # Images


def _clean_up():
    shutil.rmtree("./build")


def build():
    print("RELEASE BUILD TOOL FOR GW2RPC\n"
          "PLEASE MAKE SURE YOU ARE RUNNING THIS VIA CMD IN YOUR VENV\n\n")
    version = input("please specify version: ")
    _prepare_and_run_pyinstaller(version)
    _copy_dependencies()
    _clean_up()


if __name__ == "__main__":
    build()
