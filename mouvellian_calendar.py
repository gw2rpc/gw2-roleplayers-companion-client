###########################################
# Author: David Rodenkirchen              #
# E-Mail: davidr.develop@googlemail.com   #
# Licence: GPL-3.0-or-later               #
###########################################
import datetime

MOUVELLLIAN_SEASONS = [
    ["Wintersday", 365],
    ["Colossus", 360],
    ["Scion", 270],
    ["Phoenix", 180],
    ["Zephyr", 90]
]


def get_current_mouvellian_date():
    start_of_year = datetime.date(datetime.date.today().year, 1, 1)
    today = datetime.date.today()
    days_since_start_of_year = today - start_of_year
    days_since_start_of_year = days_since_start_of_year.days
    current_season = ""
    for season in MOUVELLLIAN_SEASONS:
        if days_since_start_of_year < season[1]:
            current_season = season[0]
            days_to_substract = season[1] - 90

    current_day = days_since_start_of_year - days_to_substract
    current_year = datetime.date.today().year - 687
    current_date = f"{current_day} {current_season} {current_year} AE"
    return current_date
