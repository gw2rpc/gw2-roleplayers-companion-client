# GW2-Roleplayers Companion

The GW2-Roleplayers Companion is a project that aims to give roleplayers in ArenaNet's MMORPG GuildWars2 a tool that allows them to more easily connect with each other and share their characters roleplay information.

## Features:

### Roleplaying preferences
Share you RP preferences with other GW2RPC users. How strict are you to the lore? What languages do you RP in? How much experience do you have? Are you currently looking for RP?

![rp_preferences](example_images/rp_preferences.png)

### Character collection
Keep track of all your characters stats and attributes, and share them with the world (if you like).

![characters](example_images/characters.png)

### Journal
Each character has a handy-dandy journal with it to write about his/her adventures in Tyria.

![journal](example_images/journal.png)

### Webview
Share your characters profile with one simple link. Or don't, that's of course up to you. (Example)

![webview](example_images/webview.png)

### Biography
There are many experiences a character might have, the biography feature gives you the option to write all of them down.

![biography](example_images/biography.png)

### Roleplayer Look-up
Have a look at other peoples characters, see who is online and looking for RP, find out which player might even be near your location.

![rp_lookup](example_images/rp_lookup.png)

### Events
Join publicly organized RP events, create your own private events and invite your friends with a secret join code. With the event feature of GW2RPC you will never ever again scratch your head, wondering where a specific event takes place.

![events](example_images/events.png)
