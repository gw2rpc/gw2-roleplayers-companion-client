###########################################
# Author: David Rodenkirchen              #
# E-Mail: davidr.develop@googlemail.com   #
# Licence: GPL-3.0-or-later               #
###########################################
import globals
import mumble_link
import backend_com
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from popups import *
from views import *
import sys
from time import sleep
import os
import configparser

globals.init_globals()


class MumbleLinkWorker(QObject):
    """
    This is a Worker-Object which sole purpose is to update the players current map on the backend.
    This worker needs to run in a thread or it will block Qt's event loop.

    Attributes
    ----------
    data_link : str
        Provides a thread-safe pipeline for the parent object to access the current map_id .
        Contains `posix` if on a linux-based OS.
    backend_communicator : Singleton instance of backend_com.BackendCom
        This Singleton offers all necessary functionality to communicate with the Backend.
    """

    def __init__(self):

        super().__init__()
        self.data_link = None
        self.backend_communicator = backend_com.BackendCom.instance()

    def run(self):
        """
        After this method is invoked, a OS check is performed. If the OS is any *nix-like OS, the function returns.
        On a NT OS, MumbleLinkWorker will poll the current data from MumbleLink and send the "map_id" value to the
        backend.

        Interval: 6 Seconds (hardcoded)

        Returns
        -------
        None (if on a linux-based operating system)

        See Also
        --------
        mumble_link.MumbleLink
        """
        while True:
            if os.name == "posix":  # Mumble Link doesnt work on Linux
                self.data_link = "posix"
                return
            fresh_data = mumble_link.get_mumble_link_info()
            if fresh_data is None:
                self.backend_communicator.set_current_map("0")
                globals.global_current_map_id = "0"
            else:
                self.backend_communicator.set_current_map(fresh_data["map_id"])
                globals.global_current_map_id = str(fresh_data["map_id"])
            self.data_link = fresh_data
            sleep(6)


class HeartbeatWorker(QObject):
    """
    This is a Worker-Object which sole purpose is to send periodical heartbeats to the backend.
    This worker needs to run in a thread or it will block Qt's event loop.

    Attributes
    ----------
    show_as_offline : bool
        Prevents a heartbeat from being sent if this is `True`. (Currently not in use)
    backend_communicator : Singleton instance of backend_com.BackendCom
        This Singleton offers all necessary functionality to communicate with the Backend.
    """

    def __init__(self):
        super().__init__()
        self.show_as_offline = False
        self.backend_communicator = backend_com.BackendCom.instance()

    def run(self):
        """
        This methods starts a endless loop that sends a heartbeat to the backend (if not prevented by `show_as_offline`)

        Interval: 10 Minutes (hardcoded)
        """
        while True:
            if not self.show_as_offline:
                self.backend_communicator.send_heartbeat()
            sleep(600)


# noinspection PyArgumentList,PyAttributeOutsideInit,PyUnresolvedReferences
class OverlayMainframe(QWidget):
    """
    OverlayMainframe is the main application windows/frame. Every Widget is a child of OverlayMainframe.

    Attributes
    ----------
    BASE_WIDTH : int
        Width of the main window.
    BASE_HEIGHT : int
        Height of the main window.
    COLLAPSED_WIDTH : int
        Width of the main window after `collapse_window()`
    COLLAPSED_HEIGHT : int
        Height of the main window after `collapse_window()`
    START_POINT : int
        Spacing between the left side of the screen and the left side of the main window.
        Gets calculated at initialization of the object. Is 400 if screen resolution width is 1900 or more, 0 if less.
    stylesheet : QFile
        QFile object that holds the stylesheets from external file `style.qss`.
    mumble_link_worker_thread : QThread
        Thread that contains the MumbleLinkWorker object.
    mumble_link_worker : MumbleLinkWorker(QObject)
        Instance of MumbleLinkWorker, see MumbleLinkWorker documentation for more info.
    mumble_link_timer : QTimer
        Endless 2-Second timer that triggers `update_mumble_data_in_my_account_view()`
    heartbeat_worker_thread : QThread
        Thread that contains the HeartbeatWorker object.
    heartbeat_worker : HeartbeatWorker(QObject)
        Instance of HeartbeatWorker, see HeartbeatWorker documentation for more info.
    side_menu_widget : QWidget
        Widget located left of the usual content window, below collapse button.
        Currently not in use.
    side_menu_layout : QHBoxLayout
        Layout for `side_menu_widget`. Currently not in use.
    main_content_widget : QWidget
        Main widget that acts as a child for all subsequent widget-views.
        All new widgets need `main_content_widget` as parent.
    main_content_layout : QHBoxLayout
        Layout for `main_content_widget` and all it's children. All widgets that need `main_content_widget` also need
        to be added to `main_content_layout` via the `addWidget()` method.
    menubar_slots : list of dicts -> {"widget": QWidget, "button_layout": QVBoxLayout, "button": QPushButton}
        A list of dictionaries. Each dictionary represents a menubar button. These dictionaries are generated by
        `views.menu_bar.generate_menu_bar_slot()`.
    my_account_view : MyAccountView(QWidget)
        This variable holds the view object for the "My Account" section
    my_characters_view : MyCharactersView(QWidget)
        This variable holds the view object for the "My Characters" section
    roleplayer_lookup_view : RoleplayerLookupView(QWidget)
        This variable holds the view object for the "Roleplayer Lookup" section
    events_view : EventsView(QWidget)
        This variable holds the view object for the "Events" section
    all_views : list of QWidgets
        All enabled/active views are added to this list at initialization. It is used by various methods to show
        or hide all views.
    """

    def __init__(self, screen_width, parent=None):
        super(OverlayMainframe, self).__init__(parent)
        if os.path.isfile("config.gw2rpc"):
            self.CONFIG = configparser.ConfigParser()
            self.CONFIG.read("config.gw2rpc")
            self.CONFIG = self.CONFIG["GENERAL"]
            align = self.CONFIG["ALIGNMENT"] if self.CONFIG["ALIGNMENT"] in ["LEFT", "CENTER", "RIGHT"] else "CENTER"
        else:
            align = "CENTER"
        self.BASE_WIDTH = 1200
        self.BASE_HEIGHT = 400
        self.COLLAPSED_WIDTH = 210
        self.COLLAPSED_HEIGHT = 40
        if align == "CENTER":
            self.START_POINT = (screen_width - self.BASE_WIDTH) // 2
        elif align == "LEFT":
            self.START_POINT = 0
        elif align == "RIGHT":
            self.START_POINT = screen_width - self.BASE_WIDTH
        self.setWindowFlags(Qt.FramelessWindowHint | Qt.WindowStaysOnTopHint)
        self.setGeometry(self.START_POINT, 0, self.BASE_WIDTH, self.BASE_HEIGHT)
        self.setWindowTitle("GW2 - Roleplayers Companion")
        self.setAttribute(Qt.WA_TranslucentBackground)
        self.stylesheet = QFile("./style.qss")
        self.stylesheet.open(QIODevice.ReadOnly)
        self.stylesheet = str(self.stylesheet.readAll(), encoding="utf-8")
        self.setStyleSheet(self.stylesheet)
        self.mumble_link_worker_thread = QThread()
        self.mumble_link_worker = MumbleLinkWorker()
        self.mumble_link_worker.moveToThread(self.mumble_link_worker_thread)
        self.mumble_link_worker_thread.started.connect(self.mumble_link_worker.run)
        self.mumble_link_worker_thread.start()
        self.mumble_link_timer = QTimer(self)
        self.mumble_link_timer.start(2000)
        self.mumble_link_timer.timeout.connect(self.update_mumble_data_in_my_account_view)
        self.heartbeat_worker_thread = QThread()
        self.heartbeat_worker = HeartbeatWorker()
        self.heartbeat_worker.moveToThread(self.heartbeat_worker_thread)
        self.heartbeat_worker_thread.started.connect(self.heartbeat_worker.run)
        self.heartbeat_worker_thread.start()
        self.side_menu_widget, self.side_menu_layout = initialize_side_menu_view(self)
        self.main_content_widget, self.main_content_layout = initialize_main_content_view(self)

        # menubar slots
        self.menubar_slots = []
        self.menubar_slots.append(generate_menu_bar_slot(self, 9, 201, "slot_1", "collapse GW2RPC", "collapse"))
        self.menubar_slots.append(generate_menu_bar_slot(self, 219, 141, "slot_2", "GW2RPC global status and settings",
                                                         "my_account"))
        self.menubar_slots.append(generate_menu_bar_slot(self, 370, 141, "slot_3", "My GW2RPC characters",
                                                         "my_characters"))
        self.menubar_slots.append(generate_menu_bar_slot(self, 520, 141, "slot_4", "My GW2RPC Companions",
                                                         "my_companions"))
        self.menubar_slots[3]["button"].setEnabled(False)  # ToDo: enable when working
        self.menubar_slots.append(generate_menu_bar_slot(self, 670, 141, "slot_5", "Roleplayer Look-Up",
                                                         "roleplayer_lookup"))
        self.menubar_slots.append(generate_menu_bar_slot(self, 820, 141, "slot_6", "Events", "event"))
        # self.menubar_slots[5]["button"].setEnabled(False)  # ToDo: enable when working
        self.menubar_slots.append(generate_menu_bar_slot(self, 970, 141, "slot_7", "Find Event", "find_event"))
        self.menubar_slots[6]["button"].setEnabled(False)  # ToDo: enable when working
        self.menubar_slots.append(generate_menu_bar_slot(self, 1120, 77, "slot_8", "Exit", "exit"))

        # my account
        self.my_account_view = MyAccountView(self.main_content_widget)
        self.main_content_layout.addWidget(self.my_account_view)

        # my characters
        self.my_characters_view = MyCharactersView(self.main_content_widget)
        self.main_content_layout.addWidget(self.my_characters_view)

        # roleplayer lookup
        self.roleplayer_lookup_view = RoleplayerLookupView(self.main_content_widget)
        self.main_content_layout.addWidget(self.roleplayer_lookup_view)

        # events
        self.events_view = EventsView(self.main_content_widget)
        self.main_content_layout.addWidget(self.events_view)

        # all views
        self.all_views = [self.my_characters_view,
                          self.my_account_view,
                          self.roleplayer_lookup_view,
                          self.events_view]
        for view in self.all_views:
            view.hide()

    def collapse_all_open_views(self):
        """
        This iterates over `all_views` and invokes the `hide()` method on all views.

        Returns
        -------
        None
        """
        for view in self.all_views:
            view.hide()

    def menu_bar_action(self, slot):
        """
        This function acts as a target for all menu bar buttons in `menubar_slots`. The `clicked` event is connected
        from those buttons to this function. This function effectively maps each button to another function, making
        swapping targets for menu bar buttons easier.

        Before mapping a buttons call to a function, it will collapse all open views if any
        button except `1` was pressed.

        Parameters
        ----------
        slot : str
            A string named `slot_n` (n being a number between 1 and 8). This parameter gets submitted by the
            lambda function of the connected button and determines its target mapped function.

        Returns
        -------
        None

        See Also
        --------
        menubar_slots
        """
        self.collapse_all_open_views() if not slot == "slot_1" else None
        if slot == "slot_1":
            self.collapse_window()
        elif slot == "slot_2":
            self.show_my_account()
        elif slot == "slot_3":
            self.show_my_characters()
        elif slot == "slot_4":
            pass
        elif slot == "slot_5":
            self.show_roleplayer_lookup_view()
        elif slot == "slot_6":
            self.show_event_view()
        elif slot == "slot_7":
            pass
        elif slot == "slot_8":
            sys.exit("exited via exit button")

    def collapse_window(self):  # ToDo: slot_1 hardcoded
        """
        `collapse_window` collapses or de-collapses the main window.
        It performs a check on the main windows current size attributes and then either:

        - hides all open views
        - hides all open views and the menu bar
        - re-establishes the original size of the main window

        If the views as well as the menubar icons are collapsed, the icon for the collapse button will be mirrored.

        Returns
        -------
        None

        ToDo
        ----
        This function is currently hardcoded to slot 1 of the menu bar.
        It should work by giving it a menu bar slot dictionary from `menu_bar_slots` as a parameter.
        """
        if self.geometry().width() <= self.COLLAPSED_WIDTH:
            self.menubar_slots[0]["button"].setIcon(QIcon(f"./views/img/icon_collapse.png"))
            self.setGeometry(self.START_POINT, 0, self.BASE_WIDTH, self.BASE_HEIGHT)
        elif self.geometry().width() > self.COLLAPSED_WIDTH and all([view.isHidden() for view in self.all_views]):
            self.menubar_slots[0]["button"].setIcon(QIcon(f"./views/img/icon_collapse_mirrored.png"))
            self.setGeometry(self.START_POINT, 0, self.COLLAPSED_WIDTH, self.COLLAPSED_HEIGHT)
        else:
            self.collapse_all_open_views()

    def show_my_account(self):
        """
        Invokes the `update_view()` and `show()` method on the object in `my_accounts_view` if the view is hidden.

        Returns
        -------
        None
        """
        if self.my_account_view.isHidden():
            self.my_account_view.update_view()
            self.my_account_view.show()

    def update_mumble_data_in_my_account_view(self):
        """
        This helper function gets triggered by `mumble_link_timer` and forwards the current contents of
        the `mumble_link_worker.data_link` to the `update_mumble_data` method of the `MyAccountView`.

        Returns
        -------
        None

        See Also
        --------
        MumbleLinkWorker
        MyAccountsView
        """
        self.my_account_view.update_mumble_data(self.mumble_link_worker.data_link)

    def show_my_characters(self):
        """
        Invokes the `show()` method on the object in `my_characters_view` if the view is hidden.

        Returns
        -------
        None
        """
        if self.my_characters_view.isHidden():
            self.my_characters_view.show()

    def button_action_4(self):
        """
        Reserved for future button targets. Not in use right now.

        Returns
        -------
        None
        """
        pass

    def show_roleplayer_lookup_view(self):
        """
        Invokes the `show()` method on the object in `roleplayer_lookup_view` if the view is hidden.

        Returns
        -------
        None
        """
        if self.roleplayer_lookup_view.isHidden():
            self.roleplayer_lookup_view.show()

    def show_event_view(self):
        """
        Invokes the `show()` method on the object in `events_view` if the view is hidden.

        Returns
        -------
        None
        """
        if self.events_view.isHidden():
            self.events_view.show()

    def button_action_7(self):
        """
        Reserved for future button targets. Not in use right now.

        Returns
        -------
        None
        """
        pass


if __name__ == '__main__':
    gw2rp_companion_main_application = QApplication(sys.argv)
    mainframe = OverlayMainframe(gw2rp_companion_main_application.primaryScreen().size().width(), parent=None)
    mainframe.show()
    gw2rp_companion_main_application.exec_()
