###########################################
# Author: David Rodenkirchen              #
# E-Mail: davidr.develop@googlemail.com   #
# Licence: GPL-3.0-or-later               #
###########################################
from PyQt5.QtCore import QRect
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QGroupBox, QLabel, QPushButton


class UserInspector(QGroupBox):
    def __init__(self):
        super().__init__()

        self.writeable_labels = {}
        self.setGeometry(QRect(0, 0, 600, 330))
        self.setFlat(True)
        self.setObjectName("UserInspector")

        def generate_label(parent_obj, size_qrect, text):
            label = QLabel(parent_obj)
            label.setGeometry(size_qrect)
            label.setText(text)
            return label

        self.writeable_labels["account_name"] = generate_label(self, QRect(20, 10, 180, 20), "")
        self.writeable_labels["account_name"].setFont(QFont('Arial', 14))

        generate_label(self, QRect(15, 50, 180, 20), "out of character status:")
        self.writeable_labels["ooc_status"] = generate_label(self, QRect(190, 50, 180, 20), "")

        generate_label(self, QRect(15, 80, 180, 20), "roleplay experience:")
        self.writeable_labels["rp_exp"] = generate_label(self, QRect(190, 80, 180, 20), "")

        generate_label(self, QRect(15, 110, 180, 20), "preferred language:")
        self.writeable_labels["pref_lang"] = generate_label(self, QRect(190, 110, 180, 20), "")

        generate_label(self, QRect(15, 140, 180, 20), "roleplay intensity:")
        self.writeable_labels["rp_intensity"] = generate_label(self, QRect(190, 140, 180, 20), "")

        generate_label(self, QRect(15, 170, 180, 20), "lore strictness:")
        self.writeable_labels["lore_strictness"] = generate_label(self, QRect(190, 170, 180, 20), "")

        generate_label(self, QRect(15, 200, 180, 20), "roleplay type preferences:")
        self.writeable_labels["rp_type_pref"] = generate_label(self, QRect(190, 200, 180, 80), "")

        self.back_button = QPushButton("back", self)
        self.back_button.setGeometry(QRect(490, 290, 100, 30))
        self.back_button.clicked.connect(self.hide)

    def update_view(self, user_data):
        if user_data["rp_type_pref"]:
            user_data["rp_type_pref"] = user_data["rp_type_pref"].replace("adv", "adventure\n")
            user_data["rp_type_pref"] = user_data["rp_type_pref"].replace("rom", "romance\n")
            user_data["rp_type_pref"] = user_data["rp_type_pref"].replace("dra", "drama\n")
            user_data["rp_type_pref"] = user_data["rp_type_pref"].replace("sol", "slice of life\n")
            user_data["rp_type_pref"] = user_data["rp_type_pref"].replace(",", "")
        for k in user_data.keys():
            if k in self.writeable_labels.keys():
                self.writeable_labels[k].setText(user_data[k])
