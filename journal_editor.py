###########################################
# Author: David Rodenkirchen              #
# E-Mail: davidr.develop@googlemail.com   #
# Licence: GPL-3.0-or-later               #
###########################################
from PyQt5.QtCore import QRect
from PyQt5.QtWidgets import QGroupBox, QTextEdit, QPushButton, QTextBrowser, QLabel
import link_opener
import format_code_parser
import os


class JournalEditor(QGroupBox):
    JOURNAL_PATH = os.path.join(f"{os.path.expandvars('%LOCALAPPDATA%')}", "GW2RPC")

    def __init__(self, parent, current_character_id, character_name):
        super().__init__()
        self.setParent(parent)
        self.setGeometry(QRect(0, 0, 960, 360))
        self.setObjectName("JournalEditor")
        self.setFlat(True)
        self.current_character_id = current_character_id
        self.current_character_journal = self.load_character_journal()

        # Header
        self.header_label = QLabel(f"<h2 style='color:#feedba;'>Journal of {character_name}</h2>", self)
        self.header_label.setGeometry(QRect(20, 10, 200, 20))

        # Editor
        self.editor = QTextEdit(self)
        self.editor.setText(self.current_character_journal if self.current_character_journal else "")
        self.editor.setGeometry(QRect(5, 50, 952, 260))
        self.editor.setAcceptRichText(False)
        self.editor.textChanged.connect(lambda: self.save_button.setEnabled(True))

        # Controls
        self.save_button = QPushButton(self)
        self.save_button.setGeometry(QRect(715, 320, 235, 30))
        self.save_button.setText("save")
        self.save_button.setEnabled(False)
        self.save_button.clicked.connect(lambda: self.save_button.setEnabled(False) if self.save_character_journal() else self.save_button.setEnabled(True))

        self.preview_button = QPushButton(self)
        self.preview_button.setGeometry(QRect(480, 320, 235, 30))
        self.preview_button.setText("read-only mode")
        self.preview_button.clicked.connect(self.previewer)

        self.back_button = QPushButton(self)
        self.back_button.setGeometry(QRect(10, 320, 235, 30))
        self.back_button.setText("back")
        self.back_button.clicked.connect(lambda: self.hide())

        self.help_button = QPushButton(self)
        self.help_button.setGeometry(QRect(245, 320, 235, 30))
        self.help_button.setText("formatting help")
        self.help_button.clicked.connect(self.show_formatting_help)

    def load_character_journal(self):
        # Make sure the local GW2RPC directory exists
        try:
            if not os.path.exists(JournalEditor.JOURNAL_PATH):
                os.makedirs(JournalEditor.JOURNAL_PATH)
        except OSError as journal_path_creation_error:
            print(f"Something went wrong while creating the journal path:\n {journal_path_creation_error}")
        try:
            with open(os.path.join(JournalEditor.JOURNAL_PATH, f"{self.current_character_id}.journal"), "r") as journal:
                return journal.read()
        except FileNotFoundError:
            print("here")
            try:
                with open(os.path.join(JournalEditor.JOURNAL_PATH, f"{self.current_character_id}.journal"), "x"):
                    return ""
            except FileExistsError:
                self.load_character_journal()

    def save_character_journal(self):
        try:
            with open(os.path.join(JournalEditor.JOURNAL_PATH, f"{self.current_character_id}.journal"), "w") as journal:
                journal.write(self.editor.toPlainText())
                return True
        except FileNotFoundError:
            return False

    def previewer(self):
        preview_box = QGroupBox(self)
        preview_box.setObjectName("JournalPreviewer")
        preview_box.setFlat(True)
        preview_box.setGeometry(QRect(0, 0, 960, 360))

        preview_text_edit = QTextBrowser(preview_box)
        preview_text_edit.setGeometry(QRect(0, 0, 960, 310))
        preview_text_edit.setOpenExternalLinks(True)
        preview_text_edit.setOpenLinks(False)
        preview_text_edit.anchorClicked.connect(lambda link: link_opener.OpenLinkWindow(preview_box, link))
        preview_text_edit.setReadOnly(True)
        text = format_code_parser.parse_formatted_code(self.editor.toPlainText())
        preview_text_edit.setText(text)

        back_button = QPushButton(preview_box)
        back_button.setGeometry(QRect(10, 320, 940, 30))
        back_button.setText("back")
        back_button.clicked.connect(lambda: preview_box.setParent(None))
        preview_box.show()

    def show_formatting_help(self):
        formatting_help_box = QGroupBox(self)
        formatting_help_box.setFlat(True)
        formatting_help_box.setGeometry(QRect(0, 0, 960, 360))

        formatting_text_edit = QTextBrowser(formatting_help_box)
        formatting_text_edit.setOpenExternalLinks(True)
        formatting_text_edit.setGeometry(QRect(0, 0, 960, 310))
        formatting_text_edit.setReadOnly(True)
        text = """
        <h1>[h1]Heading 1[/h1]</h1>
        <h2>[h2]Heading 2[/h2]</h2>
        <h3>[h3]Heading 3[/h3]</h3>
        <h4>[h4]Heading 4[/h4]</h4>
        <h5>[h5]Heading 5[/h5]</h5>
        <h6>[h6]Heading 6[/h6]</h6>
        <br>
        <i>[i]italic[/i]</i><br>
        <b>[b]bold[/b]</b><br>
        <sub>[sub]sub script[/sub]</sub><br>
        <s>[strike]strike trough[/strike]</s><br>
        <a href="#">[link:LINK]link[/link]</a><br>
        <sup>[sup]super script[/sup]</sup><br>
        <center>[c]centered[/c]</center><br>
        <small>[small]small text[/small]</small><br>
        <span style="color:DARKRED">[color:COLOR]colored text[/color]</span> <i>either <a href="https://www.w3schools.com/colors/colors_picker.asp" style="color:#CCBA1D">HEX values</a> or <a href="https://www.w3.org/wiki/CSS/Properties/color/keywords" style="color:#CCBA1D">colorcodes</a></i><br><br>
        [divider:NUMBER]<br>
        1: <img src=\"./views/img/divider1.png\"><br>
        2: <img src=\"./views/img/divider2.png\"><br>
        3: <img src=\"./views/img/divider3.png\"><br>
        4: <img src=\"./views/img/divider4.png\"><br>
        5: <img src=\"./views/img/divider5.png\"><br>
        """
        formatting_text_edit.setHtml(text)

        back_button = QPushButton(formatting_help_box)
        back_button.setGeometry(QRect(10, 320, 940, 30))
        back_button.setText("back")
        back_button.clicked.connect(lambda: formatting_help_box.setParent(None))
        formatting_help_box.show()
