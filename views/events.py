###########################################
# Author: David Rodenkirchen              #
# E-Mail: davidr.develop@googlemail.com   #
# Licence: GPL-3.0-or-later               #
###########################################
from PyQt5.QtCore import QRect, Qt, QTimer, QSize, QPoint
from PyQt5.Qt import QGuiApplication
from PyQt5.QtWidgets import QWidget, QGroupBox, QPushButton, QTextBrowser, QLabel, QLineEdit, QTextEdit, QMenu, \
    QFileDialog, QGridLayout, QScrollArea, QTreeWidget, QTreeWidgetItem, QHeaderView, QTreeWidgetItemIterator
from PyQt5.QtGui import *
from format_code_parser import parse_formatted_code
import datetime
import backend_com
import json
from random import randint


class EventsView(QWidget):
    DEFAULT_PAGE_SIZE = QRect(0, 0, 800, 313)
    DEFAULT_HOME_TEXT = """
    <center><h2>How to</h2></center>
    <p>
    This is the event section of GW2RPC. Events are social gatherings to further character growth. 
    You can create a new event or join an existing one. Below is a description of what each button in the navigation 
    does.
    </p>
    <ul>
        <li>&nbsp;&nbsp;Overview - shows this page, in case you ever need to go back<br>
        <li>&nbsp;&nbsp;Public Event List - shows a list of all ongoing or planned events that are public, you can join them from there<br>
        <li>&nbsp;&nbsp;Join Event by Code - opens a dialogue from which you can join an private event with the code you've been given<br>
        <li>&nbsp;&nbsp;Create Event - creates a new event where you are the organizer<br>
        <li>&nbsp;&nbsp;My Events - lists all events you've joined, you can enter all your event - rooms from here
    </ul>
    """

    def __init__(self, flags, *args, **kwargs):
        super().__init__(flags, *args, **kwargs)

        self.content_pages = {}
        self.navigation = []

        """ content pages UI setup """
        self.content_pages["home"] = self.generate_page("overview")
        self.content_pages["public_event_list"] = PublicEventList(self)
        self.content_pages["join_event_by_code"] = self.generate_page("join event by code")
        self.content_pages["create_event"] = EventCreateView(self)
        self.content_pages["my_events"] = EventMyEventsView(self)

        """ navigation UI setup """
        self.navigation_box = QGroupBox("", self)
        self.navigation_box.setGeometry(QRect(800, 0, 150, 313))
        self.navigation_box.setObjectName("EventsViewNavigationBox")

        navigation_margin_iterator = 0
        for page in self.content_pages:
            self.navigation.append(
                QPushButton(f"{self.content_pages[page].navigation_button_text}", self.navigation_box))
            self.navigation[-1].setGeometry(QRect(12, 67 + (navigation_margin_iterator * 50), 130, 30))
            self.navigation[-1].clicked.connect(self.navigation_button_pushed)
            self.navigation[-1].corresponding_key = page
            navigation_margin_iterator += 1

        self.content_pages["home"].textbrowser = QTextBrowser(self.content_pages["home"])
        self.content_pages["home"].textbrowser.setGeometry(QRect(40, 70, 750, 230))
        self.content_pages["home"].textbrowser.setStyleSheet("background-color: transparent; border:0")
        self.content_pages["home"].textbrowser.setHtml(EventsView.DEFAULT_HOME_TEXT)

        self.utc_time_label = QLabel(f"UTC clock: {datetime.datetime.now(datetime.timezone.utc).strftime('%H:%M')}",
                                     self.navigation_box)
        self.utc_time_label.setGeometry(QRect(30, 32, 150, 20))
        self.clock_updater = QTimer(self.navigation_box)
        self.clock_updater.timeout.connect(lambda: self.utc_time_label.setText(
            f"UTC clock: {datetime.datetime.now(datetime.timezone.utc).strftime('%H:%M')}"))
        self.clock_updater.start(5000)
        self.navigation_box.show()
        self.content_pages["home"].show()

    def navigation_button_pushed(self):
        getattr(self, f"{self.sender().corresponding_key}_func")()

    def generate_page(self, nav_btn_txt):
        """ This is just a placeholder function for empty content pages, delete later ! """
        new_page = QGroupBox("", self)
        new_page.setFlat(True)
        new_page.navigation_button_text = nav_btn_txt
        new_page.setGeometry(EventsView.DEFAULT_PAGE_SIZE)
        new_page.setObjectName("EventsViewPage")
        new_page.hide()
        return new_page

    def home_func(self):
        self._hide_all_other_pages()
        self.content_pages["home"].show()

    def public_event_list_func(self):
        self._hide_all_other_pages()
        self.content_pages["public_event_list"].show()

    def join_event_by_code_func(self):
        EventViewJoinByCode(self).show()

    def create_event_func(self):
        self._hide_all_other_pages()
        self.content_pages["create_event"].show()

    def my_events_func(self):
        self._hide_all_other_pages()
        self.content_pages["my_events"].show()

    def _hide_all_other_pages(self):
        for page in self.content_pages.values():
            page.hide()


class EventViewJoinByCode(QGroupBox):
    def __init__(self, parent, prefilled_code=None):
        super().__init__()
        self.setParent(parent)
        self.setObjectName("EventViewJoinByCode")
        self.backend_communicator = backend_com.BackendCom.instance()
        self.setGeometry(QRect(300, 100, 250, 150))
        self.header_label = QLabel("<center><h2>join event by code</h2></center>", self)
        self.header_label.setGeometry(QRect(0, 10, 250, 30))
        self.info_label = QLabel("<center><h4>please enter an event-code:</h4></center>", self)
        self.info_label.setGeometry(QRect(0, 55, 250, 20))
        self.code_lineedit = QLineEdit(self)
        self.code_lineedit.setGeometry(QRect(75, 78, 100, 30))
        self.code_lineedit.setAlignment(Qt.AlignCenter)
        self.code_lineedit.setMaxLength(6)
        if not prefilled_code:
            self.code_lineedit.setPlaceholderText("XYZ123")
        else:
            self.code_lineedit.setText(prefilled_code)
        self.abort_button = QPushButton("cancel", self)
        self.abort_button.setGeometry(QRect(5, 120, 115, 25))
        self.abort_button.clicked.connect(lambda: self.setParent(None))
        self.join_button = QPushButton("join", self)
        self.join_button.setGeometry(QRect(130, 120, 115, 25))
        self.join_button.clicked.connect(self._join_event)

    def _join_event(self):
        if self.backend_communicator.join_event(self.code_lineedit.text()):
            self.join_button.hide()
            self.abort_button.setText("back")
            self.code_lineedit.setText("")
            self.code_lineedit.setPlaceholderText("joined !")
            self.code_lineedit.setReadOnly(True)
        else:
            self.code_lineedit.setText("")
            self.code_lineedit.setPlaceholderText("code invalid")
            QTimer.singleShot(2000, lambda: self.code_lineedit.setPlaceholderText("XYZ123"))


class EventCreateView(QGroupBox):
    def __init__(self, parent):
        super().__init__()
        self.setParent(parent)
        self.setGeometry(EventsView.DEFAULT_PAGE_SIZE)
        self.backend_communicator = backend_com.BackendCom.instance()
        self.navigation_button_text = "create new event"
        self.setObjectName("EventsViewPage")

        self.header_label = QLabel("<h2 style='color:#feedba;'>create new event</h2>", self)
        self.header_label.setGeometry(QRect(190, 28, 200, 20))

        self.invalid_data_label = QLabel("", self)
        self.invalid_data_label.setGeometry(QRect(360, 285, 450, 30))

        self.description_label = QLabel("<h3>description</h3>", self)
        self.description_label.setGeometry(QRect(500, 70, 100, 20))

        self.event_name_label = QLabel("<h3>event name:</h3>", self)
        self.event_name_label.setGeometry(QRect(40, 70, 100, 20))

        self.event_publicity_label = QLabel("<h3>publicity:</h3>", self)
        self.event_publicity_label.setGeometry(QRect(40, 100, 100, 20))

        self.event_server_label = QLabel("<h3>server:</h3>", self)
        self.event_server_label.setGeometry(QRect(40, 130, 100, 20))

        self.event_location_label = QLabel("<h3>location:</h3>", self)
        self.event_location_label.setGeometry(QRect(40, 160, 100, 20))

        self.event_start_label = QLabel("<h3>start:</h3>", self)
        self.event_start_label.setGeometry(QRect(40, 190, 100, 20))

        self.event_end_label = QLabel("<h3>end:</h3>", self)
        self.event_end_label.setGeometry(QRect(40, 220, 100, 20))

        self.back_button = QPushButton("cancel", self)
        self.back_button.setGeometry(QRect(40, 270, 120, 30))
        self.back_button.clicked.connect(lambda: self.hide() or self.parent().home_func())

        self.create_button = QPushButton("create", self)
        self.create_button.setGeometry(QRect(170, 270, 120, 30))
        self.create_button.clicked.connect(self._create)

        self.export_import_menu_button = QPushButton(QIcon("./views/img/icon_my_account.png"), "", self)
        self.export_import_menu_button.setGeometry(QRect(300, 270, 30, 30))
        self.export_import_menu_button.setFlat(True)
        self.export_import_menu_button.setIconSize(QSize(22, 22))
        self.export_import_menu_button.clicked.connect(lambda:
                                                       self.export_import_menu.exec_(
                                                           self.export_import_menu_button.mapToGlobal(QPoint(5, 5))))
        self.export_import_menu_status_indicator = QLabel("", self)
        self.export_import_menu_status_indicator.setGeometry(QRect(330, 273, 30, 30))
        self.export_import_menu = QMenu(self)
        self.export_import_menu.addAction("import event ...").triggered.connect(self._import_event_from_file)
        self.export_import_menu.addAction("export event ...").triggered.connect(self._export_event_to_file)

        self.line_edits = {
            "event_name": QLineEdit(self),
            "event_location": QLineEdit(self),
            "event_start": QLineEdit(self),
            "event_end": QLineEdit(self)
        }

        self.line_edits["event_name"].setGeometry(QRect(150, 70, 160, 20))
        self.line_edits["event_name"].setMaxLength(40)
        self.line_edits["event_location"].setGeometry(QRect(150, 160, 160, 20))
        self.line_edits["event_location"].setMaxLength(15)
        self.line_edits["event_start"].setGeometry(QRect(150, 190, 160, 20))
        self.line_edits["event_start"].setPlaceholderText("2021-07-15 09:00:00")
        self.line_edits["event_start"].textChanged.connect(self._check_line_edit_for_valid_timestamp_format)
        self.line_edits["event_start"].setMaxLength(19)
        self.line_edits["event_end"].setGeometry(QRect(150, 220, 160, 20))
        self.line_edits["event_end"].setPlaceholderText("2021-07-15 14:00:00")
        self.line_edits["event_end"].textChanged.connect(self._check_line_edit_for_valid_timestamp_format)
        self.line_edits["event_end"].setMaxLength(19)

        self.event_publicity_switch = QPushButton("public", self)
        self.event_publicity_switch.setGeometry(QRect(150, 100, 160, 20))
        self.event_publicity_switch.clicked.connect(lambda: self._switch_button("publicity"))
        self.event_server_switch = QPushButton("EU", self)
        self.event_server_switch.setGeometry(QRect(150, 130, 160, 20))
        self.event_server_switch.clicked.connect(lambda: self._switch_button("server"))

        self.description_editor = QTextEdit(self)
        self.description_editor.setGeometry(QRect(360, 110, 420, 180))
        self.description_editor.textChanged.connect(self._check_description_editor_for_max_lenght)
        self.hide()

    def _check_description_editor_for_max_lenght(self):
        max_description_len = 8000
        if len(self.description_editor.toPlainText()) > max_description_len:
            self.invalid_data_label.setText(
                f"<b style='color: red;'>Your event description can not exceed {max_description_len} characters.</b>")
            QTimer.singleShot(2000, lambda: self.invalid_data_label.setText(""))
            self.description_editor.setText(self.description_editor.toPlainText()[:max_description_len])

    def _switch_button(self, switch_button):
        if switch_button == "publicity":
            if self.event_publicity_switch.text() == "public":
                self.event_publicity_switch.setText("private")
            else:
                self.event_publicity_switch.setText("public")
        elif switch_button == "server":
            if self.event_server_switch.text() == "EU":
                self.event_server_switch.setText("NA")
            else:
                self.event_server_switch.setText("EU")

    def _check_line_edit_for_valid_timestamp_format(self):
        # Example: 2021-07-12 11:40:51 // YYYY-MM-DD HH:MM:SS
        try:
            datetime.datetime.strptime(self.sender().text(), "%Y-%m-%d %H:%M:%S")
            self.sender().setStyleSheet("QLineEdit { border: 2px solid #004B00; }")
        except ValueError:
            self.sender().setStyleSheet("QLineEdit { border: 2px solid #8B0000; }")

    def _validate(self):
        new_event_payload = {
            "name": self.line_edits["event_name"].text(),
            "location": self.line_edits["event_location"].text(),
            "start": self.line_edits["event_start"].text(),
            "end": self.line_edits["event_end"].text(),
            "publicity": self.event_publicity_switch.text(),
            "server": self.event_server_switch.text(),
            "description": self.description_editor.toPlainText()
        }

        if not new_event_payload["name"]:
            self.invalid_data_label.setText("<b style='color: red;'>Your event needs to have a name</b>")
            QTimer.singleShot(2000, lambda: self.invalid_data_label.setText(""))
            return False
        try:
            datetime.datetime.strptime(new_event_payload["start"], "%Y-%m-%d %H:%M:%S")
            datetime.datetime.strptime(new_event_payload["end"], "%Y-%m-%d %H:%M:%S")
        except ValueError:
            self.invalid_data_label.setText("<b style='color: red;'>Your event needs to have valid dates</b>")
            QTimer.singleShot(2000, lambda: self.invalid_data_label.setText(""))
            return False
        return new_event_payload

    def _create(self):
        new_event_payload = self._validate()
        if new_event_payload:
            new_event_code = self.backend_communicator.create_event(new_event_payload)
            for child in self.findChildren(QWidget):
                child.hide()
            success_label = QLabel("<h2>successfully created event!</h2>", self)
            success_label.setGeometry(QRect(0, 80, 800, 60))
            success_label.setAlignment(Qt.AlignCenter)
            success_label2 = QLabel("invite other people to your event via this code:", self)
            success_label2.setGeometry(QRect(0, 105, 800, 60))
            success_label2.setAlignment(Qt.AlignCenter)
            success_label.show()
            success_label2.show()
            if not new_event_code:
                success_label.setText("<h2>There was an error creating your event</h2>")
                success_label2.setText("please report this bug to the development team")
                return
            code_line_edit = QLineEdit(new_event_code, self)
            code_line_edit.setGeometry(QRect(267, 150, 267, 40))
            code_line_edit.setAlignment(Qt.AlignCenter)
            code_line_edit_font = code_line_edit.font()
            code_line_edit_font.setPointSize(26)
            code_line_edit.setFont(code_line_edit_font)
            code_line_edit.setReadOnly(True)
            close_button = QPushButton("close", self)
            close_button.setGeometry(QRect(40, 270, 130, 30))
            close_button.clicked.connect(self._leave_and_destroy)
            code_line_edit.show()
            close_button.show()

    def _leave_and_destroy(self):
        self.parent().content_pages["create_event"] = EventCreateView(self.parent())
        self.parent().home_func()
        self.setParent(None)

    def _export_event_to_file(self):
        event_to_export = self._validate()
        if not event_to_export:
            return
        export_target_file = QFileDialog.getSaveFileName(self, "export event", "./", "json")
        export_target_file = ".".join(export_target_file)
        try:
            with open(export_target_file, "w+") as etf:
                etf.write(json.dumps(event_to_export, indent=4))
                self.export_import_menu_status_indicator.setText("<h2>✔</h2>️")
        except (PermissionError, IsADirectoryError):
            self.export_import_menu_status_indicator.setText("<h2>❌</h2>️")
        QTimer.singleShot(2000, lambda: self.export_import_menu_status_indicator.setText(""))

    def _import_event_from_file(self):
        import_target_file = QFileDialog.getOpenFileName(self, "import event", "./", "*.json")
        try:
            with open(import_target_file[0], "r") as event:
                event = json.load(event)
        except (FileNotFoundError, PermissionError):
            self.export_import_menu_status_indicator.setText("<h2>❌</h2>️")
            return

        for needed_key in ["name", "location", "start", "end", "publicity", "server", "description"]:
            if needed_key not in event.keys():
                self.export_import_menu_status_indicator.setText("<h2>❌</h2>️")
                return

        self.line_edits["event_name"].setText(event["name"])
        self.line_edits["event_location"].setText(event["location"])
        self.line_edits["event_start"].setText(event["start"])
        self.line_edits["event_end"].setText(event["end"])
        self.event_publicity_switch.setText("public" if event["publicity"] == "public" else "private")  # for security
        self.event_server_switch.setText("EU" if event["server"] == "EU" else "NA")  # for security
        self.description_editor.setText(event["description"])


class EventMyEventsView(QGroupBox):
    def __init__(self, parent):
        super().__init__()
        self.setParent(parent)
        self.setGeometry(EventsView.DEFAULT_PAGE_SIZE)
        self.backend_communicator = backend_com.BackendCom.instance()
        self.navigation_button_text = "my events"
        self.setObjectName("EventsViewPage")
        self.events = self.backend_communicator.get_joined_events()
        self.events_grid_widget = QWidget(self)
        self.events_grid_widget.setObjectName("EventsGridWidget")
        self.events_scroll_area = QScrollArea(self)
        self.events_scroll_area.setWidget(self.events_grid_widget)
        self.events_scroll_area.setGeometry(QRect(30, 60, 770, 250))
        self.events_scroll_area.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.events_scroll_area.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.events_scroll_area.setStyleSheet("QScrollArea { background-color: transparent; border:none; }")
        self.events_grid = QGridLayout()
        self.events_grid_widget.setLayout(self.events_grid)
        self.events_grid_widget.setGeometry(QRect(0, 0, 770, 250))  # default size

        self.search_bar = QLineEdit(self)
        self.search_bar.setGeometry(QRect(180, 27, 200, 25))
        self.search_bar.setObjectName("SearchBar")
        self.search_bar.textEdited.connect(self._filter_grid)
        self._fill_grid()
        # Refresh Button
        self.refresh_button = QPushButton(self)
        self.refresh_button.setIcon(QIcon("./views/img/icon_refresh.png"))
        self.refresh_button.setIconSize(QSize(18, 18))
        self.refresh_button.setFlat(True)
        self.refresh_button.setGeometry(QRect(750, 33, 18, 18))
        self.refresh_button.clicked.connect(self._update_grid)
        self.hide()

    def showEvent(self, event):  # reimplemented virtual
        self._update_grid()
        event.accept()

    def _fill_grid(self):
        if not self.events:  # No events or server offline
            return
        if len(self.events) == 1:  # corner case, calculating grid size here not possible, setting default
            self.events_grid_widget.setGeometry(QRect(0, 0, 770, self.EventCard.MAXIMUM_HEIGHT))
        else:
            self.events_grid_widget.setGeometry(QRect(0, 0, 770, (
                    self.EventCard.MAXIMUM_HEIGHT * len(self.events)) - len(self.events) * 20))
        for i, event in enumerate(self.events):
            self.events_grid.addWidget(self.EventCard(self, event), i - i % 2, i % 2)

    def _update_grid(self):
        """ deletes all cards from grid, updates the event data and re-fills the grid with updated cards"""
        while self.events_grid.count():
            self.events_grid.takeAt(0).widget().setParent(None)
        self.events = self.backend_communicator.get_joined_events()
        self._fill_grid()

    def _filter_grid(self):
        for event_card_idx in range(self.events_grid.count()):
            c_card = self.events_grid.itemAt(event_card_idx).widget()
            c_card.show() if self.search_bar.text().lower() in c_card.event_data['name'].lower() else c_card.hide()

    class EventCard(QPushButton):
        MAXIMUM_WIDTH = 325
        MAXIMUM_HEIGHT = 70

        def __init__(self, parent, event_data):
            super().__init__()
            self.setParent(parent)
            self.setObjectName(f"EventCard{str(randint(1, 4))}")
            self.event_data = event_data
            self.setText(f"{self.event_data['name']}\n"
                         f"hosted by {self.event_data['host']}\n"
                         f"{self.event_data['start']} - {self.event_data['end']}")
            self.code = self.event_data["code"]
            self.setMaximumWidth(self.MAXIMUM_WIDTH)
            self.setMaximumHeight(self.MAXIMUM_HEIGHT)
            self.clicked.connect(lambda: SingleEventView(parent, self.code))


class SingleEventView(QGroupBox):
    def __init__(self, parent, event_code):
        super().__init__()
        self.setParent(parent)
        self.setGeometry(EventsView.DEFAULT_PAGE_SIZE)
        self.backend_communicator = backend_com.BackendCom.instance()
        self.setObjectName("EventsViewPage")
        self.clipboard = QGuiApplication.clipboard()
        self.event_data = self.backend_communicator.get_single_event_data(event_code)
        if self.event_data['host'] == self.backend_communicator.get_data_from_endpoint("accounts/info")["account_name"]:
            self.is_event_host = True
        else:
            self.is_event_host = False
        self.header_label = QLabel(f"<h2 style='color:#feedba;'>{self.event_data['name']}</h2>", self)
        self.header_label.setGeometry(QRect(190, 28, 200, 20))
        # Host
        self.host_label = self._create_formatted_label("host:", QRect(40, 70, 80, 20))
        self.host_lineedit = self._create_formatted_lineedit(self.event_data['host'], QRect(100, 70, 140, 20))
        # Publicity
        self.publicity_label = self._create_formatted_label("publicity:", QRect(260, 70, 80, 20))
        if self.is_event_host:
            self.event_publicity_switch = QPushButton("public" if self.event_data['public'] else "private", self)
            self.event_publicity_switch.setGeometry(QRect(340, 70, 60, 20))
            self.event_publicity_switch.clicked.connect(lambda: self._switch_button("publicity"))
        else:
            self.publicity_lineedit = self._create_formatted_lineedit(
                "public" if self.event_data['public'] else "private", QRect(340, 70, 60, 20))
        # Server
        self.server_label = self._create_formatted_label("server:", QRect(420, 70, 80, 20))
        if self.is_event_host:
            self.event_server_switch = QPushButton(self.event_data['server'], self)
            self.event_server_switch.setGeometry(QRect(490, 70, 60, 20))
            self.event_server_switch.clicked.connect(lambda: self._switch_button("server"))
        else:
            self.server_lineedit = self._create_formatted_lineedit(self.event_data['server'], QRect(490, 70, 60, 20))
        # Location
        self.location_label = self._create_formatted_label("location:", QRect(570, 70, 80, 20))
        self.location_lineedit = self._create_formatted_lineedit(self.event_data['location'],
                                                                 QRect(650, 70, 100, 20),
                                                                 read_only=not self.is_event_host)
        self.location_lineedit.setMaxLength(15)
        self.copy_location_to_clipboard_button = QPushButton(QIcon("./views/img/copy_to_clipboard.png"), "", self)
        self.copy_location_to_clipboard_button.setGeometry(QRect(760, 68, 20, 20))
        self.copy_location_to_clipboard_button.setFlat(True)
        self.copy_location_to_clipboard_button.setIconSize(QSize(20, 20))
        self.copy_location_to_clipboard_button.clicked.connect(self._copy_location_to_clipboard)
        # Start
        self.start_label = self._create_formatted_label("start:", QRect(40, 100, 80, 20))
        self.start_lineedit = self._create_formatted_lineedit(self.event_data['start'], QRect(100, 100, 140, 20))
        # End
        self.end_label = self._create_formatted_label("end:", QRect(260, 100, 80, 20))
        self.end_lineedit = self._create_formatted_lineedit(self.event_data['end'], QRect(340, 100, 140, 20))
        # Textbrowser (actually a QTextEdit, but only editable for the event host)
        self.textbrowser_header_label = self._create_formatted_label("description", QRect(25, 120, 800, 30), size=2)
        self.textbrowser_header_label.setAlignment(Qt.AlignCenter)
        self.description_switch_button = QPushButton("description", self)
        self.description_switch_button.setGeometry(QRect(30, 125, 100, 20))
        self.description_switch_button.clicked.connect(lambda: self._switch_textbrowser_viewmode("desc"))
        self.latest_news_switch_button = QPushButton("latest news", self)
        self.latest_news_switch_button.setGeometry(QRect(140, 125, 100, 20))
        self.latest_news_switch_button.clicked.connect(lambda: self._switch_textbrowser_viewmode("news"))
        self.textbrowser = QTextEdit(parse_formatted_code(self.event_data['description']), self)
        self.current_textbrowser_mode = "desc"
        self.textbrowser.setGeometry(QRect(30, 150, 765, 160))
        self.textbrowser.setReadOnly(not self.is_event_host)
        # Save Button
        self.save_button = QPushButton(self)
        self.save_button.setIcon(QIcon("./views/img/icon_save.png"))
        self.save_button.setIconSize(QSize(18, 18))
        self.save_button.setFlat(True)
        self.save_button.setGeometry(QRect(723, 33, 18, 18))
        self.save_button.clicked.connect(self._update_event_on_server)
        self.save_button.setHidden(not self.is_event_host)
        # Delete Button
        if self.is_event_host:
            self.delete_button = QPushButton(self)
            self.delete_button.setIcon(QIcon("./views/img/icon_trash.png"))
            self.delete_button.setIconSize(QSize(18, 18))
            self.delete_button.setFlat(True)
            self.delete_button.setGeometry(QRect(696, 33, 18, 18))
            self.delete_button.clicked.connect(self._delete)
        else:
            self.leave_button = QPushButton(self)
            self.leave_button.setIcon(QIcon("./views/img/icon_leave.png"))
            self.leave_button.setIconSize(QSize(18, 18))
            self.leave_button.setFlat(True)
            self.leave_button.setGeometry(QRect(696, 33, 18, 18))
            self.leave_button.clicked.connect(self._leave)
        # Refresh Button
        self.refresh_button = QPushButton(self)
        self.refresh_button.setIcon(QIcon("./views/img/icon_refresh.png"))
        self.refresh_button.setIconSize(QSize(18, 18))
        self.refresh_button.setFlat(True)
        self.refresh_button.setGeometry(QRect(750, 33, 18, 18))
        self.refresh_button.clicked.connect(self._update)
        # Exit View Button
        self.exit_view_button = QPushButton(self)
        self.exit_view_button.setIcon(QIcon("./views/img/icon_exit.png"))
        self.exit_view_button.setIconSize(QSize(18, 18))
        self.exit_view_button.setFlat(True)
        self.exit_view_button.setGeometry(QRect(777, 33, 18, 18))
        self.exit_view_button.clicked.connect(lambda: self.setParent(None))
        self.show()

    def _switch_button(self, switch_button):
        if switch_button == "publicity":
            if self.event_publicity_switch.text() == "public":
                self.event_publicity_switch.setText("private")
            else:
                self.event_publicity_switch.setText("public")
        elif switch_button == "server":
            if self.event_server_switch.text() == "EU":
                self.event_server_switch.setText("NA")
            else:
                self.event_server_switch.setText("EU")

    def _create_formatted_label(self, text, geometry, size=3):
        new_label = QLabel(f"<h{size}><b>{text}</b></h{size}>", self)
        new_label.setGeometry(geometry)
        return new_label

    def _create_formatted_lineedit(self, text, geometry, centered=True, read_only=True):
        new_lineedit = QLineEdit(f"{text}", self)
        new_lineedit.setGeometry(geometry)
        if centered:
            new_lineedit.setAlignment(Qt.AlignCenter)
        new_lineedit.setReadOnly(read_only)
        return new_lineedit

    def _copy_location_to_clipboard(self):
        location = self.location_lineedit.text()
        self.clipboard.setText(location)
        self.location_lineedit.setText("- copied -")
        QTimer.singleShot(1000, lambda: self.location_lineedit.setText(location))

    def _switch_textbrowser_viewmode(self, mode):
        if mode == "news" and self.current_textbrowser_mode == "desc":
            self.event_data['description'] = self.textbrowser.toPlainText()
            self.textbrowser_header_label.setText("<h2><b>latest news</h2></b>")
            self.textbrowser.setText(parse_formatted_code(self.event_data['latest_news']))
            self.current_textbrowser_mode = "news"
        elif mode == "desc" and self.current_textbrowser_mode == "news":
            self.event_data['latest_news'] = self.textbrowser.toPlainText()
            self.textbrowser_header_label.setText("<h2><b>description</h2></b>")
            self.textbrowser.setText(parse_formatted_code(self.event_data['description']))
            self.current_textbrowser_mode = "desc"
        else:
            return

    def _update(self):
        self.event_data = self.backend_communicator.get_single_event_data(self.event_data["code"])
        self.header_label.setText(f"<h2 style='color:#feedba;'>{self.event_data['name']}</h2>")
        self.host_lineedit.setText(self.event_data['host'])
        if self.is_event_host:
            self.event_publicity_switch.setText("public" if self.event_data['public'] else "private")
            self.event_server_switch.setText(self.event_data['server'])
        else:
            self.publicity_lineedit.setText("public" if self.event_data['public'] else "private")
            self.server_lineedit.setText(self.event_data['server'])
        self.location_lineedit.setText(self.event_data['location'])
        self.start_lineedit.setText(self.event_data['start'])
        self.end_lineedit.setText(self.event_data['end'])
        self._switch_textbrowser_viewmode("desc")

    def _update_event_on_server(self):
        # This is a workaround to make sure all text is saved.
        # Looks ugly as fuck, but it's faster than any other solution I came up with
        self._switch_textbrowser_viewmode("news" if self.current_textbrowser_mode == "desc" else "desc")
        self._switch_textbrowser_viewmode("news" if self.current_textbrowser_mode == "desc" else "desc")

        event_payload = {
            "code": self.event_data["code"],
            "description": self.event_data['description'],
            "end": self.event_data['end'],
            "host": self.event_data['host'],
            "latest_news": self.event_data['latest_news'],
            "location": self.location_lineedit.text(),
            "name": self.event_data['name'],
            "public": 1 if self.event_publicity_switch.text() == "public" else 0,
            "server": self.event_server_switch.text(),
            "start": self.event_data['start']
        }
        if self.backend_communicator.update_event(event_payload):
            print("function \"_update_event_on_server\" successful")
        else:
            print("function \"_update_event_on_server\" failed")

    # noinspection PyProtectedMember
    def _delete(self):
        def do_delete():
            if self.backend_communicator.delete_event(self.event_data['code']):
                self.parent()._update_grid()
                self.setParent(None)
        confirmation_box = QGroupBox(self)
        confirmation_box.setObjectName("EventViewJoinByCode")
        confirmation_box.setGeometry(QRect(300, 100, 250, 150))
        confirmation_box_header_label = QLabel("<center><h2>delete event</h2></center>", confirmation_box)
        confirmation_box_header_label.setGeometry(QRect(0, 10, 250, 30))
        confirmation_box_info_label = QLabel("<center><h4>Are you sure you want to<br>delete this event?</h4></center>", confirmation_box)
        confirmation_box_info_label.setGeometry(QRect(0, 55, 250, 40))
        confirmation_box_cancel_button = QPushButton("abort", confirmation_box)
        confirmation_box_cancel_button.setGeometry(QRect(5, 120, 115, 25))
        confirmation_box_cancel_button.clicked.connect(lambda: confirmation_box.setParent(None))
        confirmation_box_delete_button = QPushButton("delete", confirmation_box)
        confirmation_box_delete_button.setGeometry(QRect(130, 120, 115, 25))
        confirmation_box_delete_button.clicked.connect(do_delete)
        confirmation_box.show()

    # noinspection PyProtectedMember
    def _leave(self):
        def do_leave():
            if self.backend_communicator.leave_event(self.event_data['code']):
                self.parent()._update_grid()
                self.setParent(None)
        confirmation_box = QGroupBox(self)
        confirmation_box.setObjectName("EventViewJoinByCode")
        confirmation_box.setGeometry(QRect(300, 100, 250, 150))
        confirmation_box_header_label = QLabel("<center><h2>leave event</h2></center>", confirmation_box)
        confirmation_box_header_label.setGeometry(QRect(0, 10, 250, 30))
        confirmation_box_info_label = QLabel("<center><h4>Are you sure you want to<br>leave this event?</h4></center>", confirmation_box)
        confirmation_box_info_label.setGeometry(QRect(0, 55, 250, 40))
        confirmation_box_cancel_button = QPushButton("abort", confirmation_box)
        confirmation_box_cancel_button.setGeometry(QRect(5, 120, 115, 25))
        confirmation_box_cancel_button.clicked.connect(lambda: confirmation_box.setParent(None))
        confirmation_box_leave_button = QPushButton("leave", confirmation_box)
        confirmation_box_leave_button.setGeometry(QRect(130, 120, 115, 25))
        confirmation_box_leave_button.clicked.connect(do_leave)
        confirmation_box.show()


class PublicEventList(QGroupBox):
    def __init__(self, parent):
        super().__init__()
        self.setParent(parent)
        self.setGeometry(EventsView.DEFAULT_PAGE_SIZE)
        self.backend_communicator = backend_com.BackendCom.instance()
        self.public_events = list()
        self.setObjectName("EventsViewPage")
        self.navigation_button_text = "public event list"

        self.event_list = QTreeWidget(self)
        self.event_list.setObjectName("PublicEventList")
        self.event_list.setHeaderLabels(["event name", "start", "end", "code"])
        self.event_list.setAlternatingRowColors(True)
        self.event_list.setGeometry(QRect(35, 65, 760, 240))
        self.event_list.itemDoubleClicked.connect(lambda event_item: self._trigger_join_dialogue(event_item))

        self._update_list()

        self.event_list_header = self.event_list.header()
        self.event_list_header.setObjectName("PublicEventListHeader")  # ToDo: doesn't work, no idea why
        self.event_list_header.setSectionResizeMode(0, QHeaderView.Stretch)
        self.event_list_header.setSectionResizeMode(1, QHeaderView.Fixed)
        self.event_list_header.resizeSection(1, 140)
        self.event_list_header.setSectionResizeMode(2, QHeaderView.Fixed)
        self.event_list_header.resizeSection(2, 140)
        self.event_list_header.setSectionResizeMode(3, QHeaderView.Fixed)
        self.event_list_header.resizeSection(3, 80)
        self.event_list_header.setStretchLastSection(False)

        # Refresh Button
        self.refresh_button = QPushButton(self)
        self.refresh_button.setIcon(QIcon("./views/img/icon_refresh.png"))
        self.refresh_button.setIconSize(QSize(18, 18))
        self.refresh_button.setFlat(True)
        self.refresh_button.setGeometry(QRect(750, 33, 18, 18))
        self.refresh_button.clicked.connect(self._update_list)

        # Searchbar
        self.search_bar = QLineEdit(self)
        self.search_bar.setGeometry(QRect(180, 27, 200, 25))
        self.search_bar.setObjectName("SearchBar")
        self.search_bar.textEdited.connect(self._filter_list)
        self.hide()

    def _update_list(self):
        self.event_list.clear()
        self.public_events = [QTreeWidgetItem([e['name'], e['start'], e['end'], e['code']])
                              for e in self.backend_communicator.get_all_public_events()]
        self.event_list.addTopLevelItems(self.public_events)

    def _trigger_join_dialogue(self, event_item):
        EventViewJoinByCode(self, event_item.text(3)).show()

    def _filter_list(self):
        event_list_iterator = QTreeWidgetItemIterator(self.event_list)
        while event_list_iterator.value():
            li = event_list_iterator.value()
            li.setHidden(False) if self.search_bar.text().lower() in li.text(0).lower() else li.setHidden(True)
            event_list_iterator += 1
