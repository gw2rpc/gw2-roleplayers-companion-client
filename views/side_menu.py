###########################################
# Author: David Rodenkirchen              #
# E-Mail: davidr.develop@googlemail.com   #
# Licence: GPL-3.0-or-later               #
###########################################
from PyQt5.QtCore import QRect
from PyQt5.QtWidgets import QWidget, QHBoxLayout


def initialize_side_menu_view(self):
    side_menu_widget = QWidget(self)
    side_menu_widget.setGeometry(QRect(9, 39, 201, 751))
    side_menu_widget.setObjectName("side_menu_widget")
    side_menu_layout = QHBoxLayout(side_menu_widget)
    side_menu_layout.setContentsMargins(0, 0, 0, 0)
    side_menu_layout.setObjectName("side_menu_layout")
    return side_menu_widget, side_menu_layout