###########################################
# Author: David Rodenkirchen              #
# E-Mail: davidr.develop@googlemail.com   #
# Licence: GPL-3.0-or-later               #
###########################################
from PyQt5.QtCore import QRect
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QWidget, QListWidget, QPushButton, QLabel, QGroupBox
import globals
import backend_com
import popups
from character_inspection import CharacterInspector
from user_inspection import UserInspector


class RoleplayerLookupView(QWidget):
    def __init__(self, flags, *args, **kwargs):
        super().__init__(flags, *args, **kwargs)
        self.backend_communicator = backend_com.BackendCom.instance()

        self.view_mode = "online"  # "map"

        self.list_widget = QListWidget(self)
        self.list_widget.setFixedWidth(600)
        self.list_widget.setFixedHeight(330)
        self.list_widget.setAlternatingRowColors(True)
        self.list_widget.setObjectName("RoleplayerLookupList")
        self.list_widget_label = QLabel("Roleplayer Look-up", self)
        self.list_widget_label.setFont(QFont('Arial', 14))
        self.list_widget_label.setGeometry(QRect(15, 7, 170, 30))

        """ control panel configuration """
        self.control_panel = QGroupBox(self)
        self.control_panel.setFlat(True)
        self.control_panel.setObjectName("RoleplayerLookupViewControlPanel")
        self.control_panel.setGeometry(QRect(600, 0, 200, 330))
        self.control_panel_view_mode_label = QLabel(f"current view-mode: {self.view_mode}", self.control_panel)
        self.control_panel_view_mode_label.setGeometry(QRect(20, 3, 160, 20))

        self.control_panel_buttons = {
            "view_char": QPushButton("view character", self.control_panel),
            "view_player": QPushButton("view player status", self.control_panel),
            "update_list": QPushButton("update list", self.control_panel),
            "switch_mode": QPushButton("switch view-mode", self.control_panel)
        }
        self.control_panel_buttons["view_char"].setGeometry(QRect(20, 50, 170, 30))
        self.control_panel_buttons["view_char"].setEnabled(False)
        self.control_panel_buttons["view_player"].setGeometry(QRect(20, 110, 170, 30))
        self.control_panel_buttons["view_player"].setEnabled(False)
        self.control_panel_buttons["update_list"].setGeometry(QRect(20, 170, 170, 30))
        self.control_panel_buttons["switch_mode"].setGeometry(QRect(20, 280, 170, 30))

        """ connections """
        self.list_widget.itemSelectionChanged.connect(self._enable_buttons_on_list_selection)
        self.control_panel_buttons["update_list"].clicked.connect(self.update_list_with_all_online_characters)
        self.control_panel_buttons["view_char"].clicked.connect(self.open_character_inspector)
        self.control_panel_buttons["view_player"].clicked.connect(self.open_user_inspector)
        self.control_panel_buttons["switch_mode"].clicked.connect(self._switch_view_mode_and_update)

        self.inspection_box = CharacterInspector()
        self.inspection_box.setParent(self)
        self.inspection_box.hide()
        self.user_inspection_box = UserInspector()
        self.user_inspection_box.setParent(self)
        self.user_inspection_box.hide()

        self.update_list_with_all_online_characters()

    def update_list_with_all_online_characters(self):
        self.list_widget.clear()
        if self.view_mode == "online":
            self.list_widget.addItems([f"{uname[0]} as {uname[1]}" for uname in
                                       self.backend_communicator.get_all_online_players()])
        else:
            self.list_widget.addItems([f"{uname[0]} as {uname[1]}" for uname in
                                       self.backend_communicator.get_all_online_players_on_map(
                                           globals.global_current_map_id)])
        self._disable_buttons_on_list_update()

    def _switch_view_mode_and_update(self):
        self.view_mode = "online" if self.view_mode == "map" else "map"
        self.control_panel_view_mode_label.setText(f"current view-mode: {self.view_mode}")
        self.update_list_with_all_online_characters()

    def _enable_buttons_on_list_selection(self):
        self.control_panel_buttons["view_char"].setEnabled(True)
        self.control_panel_buttons["view_player"].setEnabled(True)

    def _disable_buttons_on_list_update(self):
        self.control_panel_buttons["view_char"].setEnabled(False)
        self.control_panel_buttons["view_player"].setEnabled(False)

    def _get_user_name_from_list(self):
        user_name = self.list_widget.currentItem().text()  # this includes the "as CHARACTERNAME"
        return user_name.split(" as")[0]  # split at " as", so first index is the username

    def open_character_inspector(self):
        self.user_inspection_box.hide()
        user_name = self._get_user_name_from_list()
        if user_name is None:
            return
        active_character_data = self.backend_communicator.get_active_character_by_account_name(user_name)
        if not active_character_data:
            popups.show_error_popup("no active character", "This user does not have an active character.")
            return

        self.inspection_box.update_view(active_character_data[0])
        self.inspection_box.show()

    def open_user_inspector(self):
        user_name = self._get_user_name_from_list()
        self.user_inspection_box.show()
        self.user_inspection_box.update_view(self.backend_communicator.get_user_status(user_name))

