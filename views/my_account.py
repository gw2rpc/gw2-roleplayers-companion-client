###########################################
# Author: David Rodenkirchen              #
# E-Mail: davidr.develop@googlemail.com   #
# Licence: GPL-3.0-or-later               #
###########################################
from PyQt5.QtCore import QRect, QUrl
from PyQt5.QtGui import QFont, QPixmap, QDesktopServices
from PyQt5.QtWidgets import QLabel, QPushButton, QLineEdit, QGroupBox, QCheckBox, QComboBox

import backend_com
from popups import *
import mouvellian_calendar
import json
import globals

LANGUAGES = ["english", "german", "french", "chinese", "spanish", "russian", "arabic", "japanese", "italian"]

with open("map_ids.json", "r") as map_id_file:
    MAP_IDS = json.loads(map_id_file.read())


def _generate_label(parent_obj, dimensions_qrect, label_name):
    label = QLabel(parent_obj)
    label.setGeometry(dimensions_qrect)
    label.setObjectName(label_name)
    return label


def _generate_checkbox(parent, x, y, label):
    checkbox = QCheckBox(label, parent)
    checkbox.setGeometry(QRect(x, y, 120, 31))
    return checkbox


def _generate_drop_down(parent, dimensions_qrect, obj_name):
    drop_down = QComboBox(parent)
    drop_down.setGeometry(dimensions_qrect)
    drop_down.setObjectName(obj_name)
    return drop_down


class MyAccountView(QWidget):
    def __init__(self, flags, *args, **kwargs):
        super().__init__(flags, *args, **kwargs)
        # dictionarys for labels, buttons, line edits, checkboxes and dropdowns - for easy access down the line
        self.labels = {}
        self.buttons = {}
        self.line_edits = {}
        self.drop_downs = {}
        self.checkboxes = {}

        self.backend_communicator = backend_com.BackendCom.instance()

        self.groupbox_dimensions = {"left": QRect(0, 0, 480, 360), "right": QRect(490, 0, 480, 360)}

        self.account_info_box = QGroupBox(self)
        self.account_info_box.setObjectName("AccountInfo")
        self.account_info_box.setFlat(True)
        self.account_info_box.setGeometry(self.groupbox_dimensions["right"])
        self.account_info_box_title_label = QLabel("account information", self.account_info_box)
        self.account_info_box_title_label.setGeometry(QRect(10, 5, 300, 30))
        self.account_info_box_title_label.setFont(QFont('Arial', 14))

        # account name
        self.labels["account_name"] = _generate_label(self.account_info_box, QRect(10, 55, 181, 20),
                                                      "account_name_label")
        self.labels["account_name"].setText("account name")
        self.line_edits["account_name"] = QLineEdit(self.account_info_box)
        self.line_edits["account_name"].setGeometry(QRect(200, 55, 241, 20))
        self.line_edits["account_name"].setObjectName("account_name_line_edit")
        self.line_edits["account_name"].setText("")
        self.line_edits["account_name"].setEnabled(False)
        # account status
        self.labels["account_status"] = _generate_label(self.account_info_box, QRect(10, 95, 181, 20),
                                                        "account_status_label")
        self.labels["account_status"].setText("account status")
        self.line_edits["account_status"] = QLineEdit(self.account_info_box)
        self.line_edits["account_status"].setGeometry(QRect(200, 95, 241, 20))
        self.line_edits["account_status"].setObjectName("account_status_line_edit")
        self.line_edits["account_status"].setText("")
        self.line_edits["account_status"].setEnabled(False)
        # mumble link status
        self.labels["mumble_link_status"] = _generate_label(self.account_info_box, QRect(10, 135, 181, 20),
                                                            "mumble_link_status_label")
        self.labels["mumble_link_status"].setText("mumble link status")
        self.line_edits["mumble_link_status"] = QLineEdit(self.account_info_box)
        self.line_edits["mumble_link_status"].setGeometry(QRect(200, 135, 241, 20))
        self.line_edits["mumble_link_status"].setObjectName("mumble_link_status_line_edit")
        self.line_edits["mumble_link_status"].setText("")
        self.line_edits["mumble_link_status"].setEnabled(False)
        # current map
        self.labels["current_map"] = _generate_label(self.account_info_box, QRect(10, 175, 181, 20),
                                                     "current_map_label")
        self.labels["current_map"].setText("current map")
        self.line_edits["current_map"] = QLineEdit(self.account_info_box)
        self.line_edits["current_map"].setGeometry(QRect(200, 175, 241, 20))
        self.line_edits["current_map"].setObjectName("current_map_line_edit")
        self.line_edits["current_map"].setText("")
        self.line_edits["current_map"].setEnabled(False)
        # current ingame date
        self.labels["current_ingame_date"] = _generate_label(self.account_info_box, QRect(10, 215, 181, 20),
                                                             "current_ingame_date_label")
        self.labels["current_ingame_date"].setText("current ingame date")
        self.line_edits["current_ingame_date"] = QLineEdit(self.account_info_box)
        self.line_edits["current_ingame_date"].setGeometry(QRect(200, 215, 241, 20))
        self.line_edits["current_ingame_date"].setObjectName("current_ingame_date_line_edit")
        self.line_edits["current_ingame_date"].setText(mouvellian_calendar.get_current_mouvellian_date())
        self.line_edits["current_ingame_date"].setEnabled(False)
        # update global settings button
        self.buttons["update_global_settings"] = QPushButton(self.account_info_box)
        self.buttons["update_global_settings"].setGeometry(QRect(10, 310, 460, 30))
        self.buttons["update_global_settings"].setObjectName("update_global_settings_button")
        self.buttons["update_global_settings"].setText("update global settings")
        self.buttons["update_global_settings"].setEnabled(False)
        self.buttons["update_global_settings"].clicked.connect(self.update_account)

        self.global_settings_box = QGroupBox(self)
        self.global_settings_box.setObjectName("GlobalSettings")
        if self.backend_communicator.GW2RPC_MODE == "DEBUG":
            debug_warn_label = QLabel(self.global_settings_box)
            debug_warn_pixmap = QPixmap("./views/img/debug_warning.png")
            debug_warn_label.setPixmap(debug_warn_pixmap)
            debug_warn_label.setGeometry(QRect(370, 250, 100, 100))
        elif globals.VERSION < self.backend_communicator.get_latest_version():
            new_version_available_button = QPushButton("There is a new version of GW2RPC available"
                                                       "\nClick here to download it.", self.global_settings_box)
            new_version_available_button.setObjectName("NewVersionAvailableButton")
            new_version_available_button.setGeometry(QRect(230, 280, 240, 50))
            new_version_available_button.clicked.connect(lambda: QDesktopServices.openUrl(
                QUrl("https://gw2rpc.net/download")))
        self.global_settings_box.setFlat(True)
        self.global_settings_box.setGeometry(self.groupbox_dimensions["left"])
        self.global_settings_box_title_label = QLabel("global settings", self.global_settings_box)
        self.global_settings_box_title_label.setGeometry(QRect(10, 5, 300, 30))
        self.global_settings_box_title_label.setFont(QFont('Arial', 14))

        # ooc status
        self.labels["ooc_status"] = _generate_label(self.global_settings_box, QRect(10, 55, 181, 31),
                                                    "ooc_status_label")
        self.labels["ooc_status"].setText("out of character status")
        self.drop_downs["ooc_status"] = _generate_drop_down(self.global_settings_box, QRect(220, 55, 211, 31),
                                                            "ooc_status")
        self.drop_downs["ooc_status"].addItems(["out of character - not interested",
                                                "out of character - interested",
                                                "in character - open",
                                                "in character - busy",
                                                "away from keyboard"])
        self.drop_downs["ooc_status"].setCurrentIndex(-1)
        # roleplayer experience
        self.labels["roleplayer_exp"] = _generate_label(self.global_settings_box, QRect(10, 95, 181, 31),
                                                        "roleplayer_exp_label")
        self.labels["roleplayer_exp"].setText("roleplayer experience")
        self.drop_downs["roleplayer_exp"] = _generate_drop_down(self.global_settings_box, QRect(220, 95, 211, 31),
                                                                "roleplayer_exp")
        self.drop_downs["roleplayer_exp"].addItems(["beginner", "intermediate", "experienced"])
        self.drop_downs["roleplayer_exp"].setCurrentIndex(-1)
        # roleplayer preferred language
        self.labels["roleplayer_preferred_language"] = _generate_label(self.global_settings_box,
                                                                       QRect(10, 135, 181, 31),
                                                                       "roleplayer_preferred_language_label")
        self.labels["roleplayer_preferred_language"].setText("preferred language")
        self.drop_downs["roleplayer_preferred_language"] = _generate_drop_down(self.global_settings_box,
                                                                               QRect(220, 135, 211, 31),
                                                                               "roleplayer_preferred_language")
        self.drop_downs["roleplayer_preferred_language"].addItems(sorted(LANGUAGES))
        self.drop_downs["roleplayer_preferred_language"].setCurrentIndex(-1)
        # roleplaying intensity
        self.labels["roleplaying_intensity"] = _generate_label(self.global_settings_box, QRect(10, 175, 181, 31),
                                                               "roleplaying_intensity_label")
        self.labels["roleplaying_intensity"].setText("roleplay intensity")
        self.drop_downs["roleplaying_intensity"] = _generate_drop_down(self.global_settings_box,
                                                                       QRect(220, 175, 211, 31),
                                                                       "roleplaying_intensity")
        self.drop_downs["roleplaying_intensity"].addItems(["light", "medium", "heavy"])
        self.drop_downs["roleplaying_intensity"].setCurrentIndex(-1)
        # lore strict
        self.labels["lore_strictness"] = _generate_label(self.global_settings_box, QRect(10, 215, 181, 31),
                                                         "lore_strictness_label")
        self.labels["lore_strictness"].setText("lore strictness")
        self.drop_downs["lore_strictness"] = _generate_drop_down(self.global_settings_box, QRect(220, 215, 211, 31),
                                                                 "lore_strictness_box")
        self.drop_downs["lore_strictness"].addItems(["strict", "semi-strict", "neutral", "loose", "none"])
        self.drop_downs["lore_strictness"].setCurrentIndex(-1)
        # roleplay type preference
        self.labels["roleplay_type_pref"] = _generate_label(self.global_settings_box, QRect(10, 255, 181, 31),
                                                            "roleplay_type_pref_label")
        self.labels["roleplay_type_pref"].setText("roleplay type preferences:")
        self.checkboxes["rp_type_adventure"] = _generate_checkbox(self.global_settings_box, 10, 285, "adventure")
        self.checkboxes["rp_type_romance"] = _generate_checkbox(self.global_settings_box, 150, 285, "romance")
        self.checkboxes["rp_type_drama"] = _generate_checkbox(self.global_settings_box, 150, 315, "drama")
        self.checkboxes["rp_type_slice_of_life"] = _generate_checkbox(self.global_settings_box, 10, 315,
                                                                      "slice of life")

    def update_view(self):
        up = self.backend_communicator.get_data_from_endpoint("accounts/info")
        if not up:
            self.line_edits["account_status"].setText("NOT LOGGED IN")
        else:
            # enable update button only when user is logged in
            self.buttons["update_global_settings"].setEnabled(True)
            # update view
            self.line_edits["account_status"].setText("LOGGED IN")
            self.line_edits["account_name"].setText(up["account_name"])
            self.drop_downs["ooc_status"].setCurrentText(up["ooc_status"])
            self.drop_downs["roleplayer_exp"].setCurrentText(up["rp_exp"])
            self.drop_downs["roleplayer_preferred_language"].setCurrentText(up["pref_lang"])
            self.drop_downs["roleplaying_intensity"].setCurrentText(up["rp_intensity"])
            self.drop_downs["lore_strictness"].setCurrentText(up["lore_strictness"])
            if up["rp_type_pref"] == "none" or up["rp_type_pref"] is None:
                pass
            else:
                self.checkboxes["rp_type_adventure"].setChecked("adv" in up["rp_type_pref"])
                self.checkboxes["rp_type_romance"].setChecked("rom" in up["rp_type_pref"])
                self.checkboxes["rp_type_drama"].setChecked("dra" in up["rp_type_pref"])
                self.checkboxes["rp_type_slice_of_life"].setChecked("sol" in up["rp_type_pref"])

    def update_account(self):
        rp_type_pref = []
        self.checkboxes["rp_type_adventure"].isChecked() and rp_type_pref.append("adv")
        self.checkboxes["rp_type_romance"].isChecked() and rp_type_pref.append("rom")
        self.checkboxes["rp_type_drama"].isChecked() and rp_type_pref.append("dra")
        self.checkboxes["rp_type_slice_of_life"].isChecked() and rp_type_pref.append("sol")
        rp_type_pref = ",".join(rp_type_pref) if len(rp_type_pref) > 0 else "none"
        update_payload = {
            "ooc_status": self.drop_downs["ooc_status"].currentText(),
            "rp_exp": self.drop_downs["roleplayer_exp"].currentText(),
            "pref_lang": self.drop_downs["roleplayer_preferred_language"].currentText(),
            "lore_strictness": self.drop_downs["lore_strictness"].currentText(),
            "rp_type_pref": rp_type_pref,
            "rp_intensity": self.drop_downs["roleplaying_intensity"].currentText()
        }
        if self.backend_communicator.send_payload_to_endpoint("accounts/update", update_payload):
            self.update_view()
        else:
            show_error_popup("An error has occurred", "GW2RPC could not update your account. "
                                                      "If this problem persists, consider filing an error report.")

    def update_mumble_data(self, mumble_data):
        if mumble_data is None:
            self.line_edits["mumble_link_status"].setText("INACTIVE")
            self.line_edits["current_map"].setText("GUILD WARS 2 NOT RUNNING")
        elif mumble_data == "posix":  # Mumble Link doesnt work on linux
            self.line_edits["mumble_link_status"].setText("NOT AVAILABLE ON LINUX")
        else:
            try:
                _current_map_id = str(mumble_data["map"])
                self.line_edits["mumble_link_status"].setText("ACTIVE")
                self.line_edits["current_map"].setText(MAP_IDS[_current_map_id])
            except (AttributeError, TypeError) as e:  # When worker ain't running
                print(f"error while reading mumble link data:\n{e}")
                self.line_edits["mumble_link_status"].setText("INACTIVE")
                self.line_edits["current_map"].setText("GUILD WARS 2 NOT RUNNING")
            except KeyError:  # When a unknown map-id is asked
                print(f"map id {str(mumble_data['map'])} not found")
                self.line_edits["current_map"].setText("UNKNOWN: PROBABLY SOME EOD MAP")
