###########################################
# Author: David Rodenkirchen              #
# E-Mail: davidr.develop@googlemail.com   #
# Licence: GPL-3.0-or-later               #
###########################################
from PyQt5.QtCore import QRect, QSize
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QPushButton, QWidget, QVBoxLayout


def generate_menu_bar_slot(self, xpos, xwidth, objname, hint_text, icon_name):
    widget = QWidget(self)
    widget.setGeometry(QRect(xpos, -1, xwidth, 31))
    widget.setObjectName(f"{objname}_widget")
    button_layout = QVBoxLayout(widget)
    button_layout.setContentsMargins(0, 0, 0, 0)
    button_layout.setObjectName(f"{objname}_layout")
    button = QPushButton(self)
    button.setObjectName(f"{objname}_button")
    button.setToolTip(hint_text)
    button.setIcon(QIcon(f"./views/img/icon_{icon_name}.png"))
    button.setFlat(True)
    button.setIconSize(QSize(21, 21))
    button.clicked.connect(lambda: self.menu_bar_action(objname))
    button_layout.addWidget(button)
    return {"widget": widget, "button_layout": button_layout, "button": button}
