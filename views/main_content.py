###########################################
# Author: David Rodenkirchen              #
# E-Mail: davidr.develop@googlemail.com   #
# Licence: GPL-3.0-or-later               #
###########################################
from PyQt5.QtCore import QRect
from PyQt5.QtWidgets import QWidget, QHBoxLayout


def initialize_main_content_view(self):
    main_content_widget = QWidget(self)
    main_content_widget.setGeometry(QRect(219, 39, 971, 751))
    main_content_widget.setObjectName("main_content_widget")
    main_content_layout = QHBoxLayout(main_content_widget)
    main_content_layout.setContentsMargins(0, 0, 0, 0)
    main_content_layout.setObjectName("main_content_layout")
    return main_content_widget, main_content_layout
