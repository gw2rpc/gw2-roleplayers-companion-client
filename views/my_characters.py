###########################################
# Author: David Rodenkirchen              #
# E-Mail: davidr.develop@googlemail.com   #
# Licence: GPL-3.0-or-later               #
###########################################
from PyQt5.QtCore import QRect, QSize, Qt
from PyQt5.QtWidgets import QLabel, QWidget, QLineEdit, QGroupBox, QTabWidget, QPushButton, QCheckBox, QInputDialog, \
    QRadioButton
from PyQt5.QtGui import QIcon
import backend_com
from biography_editor import BiographyEditor
from journal_editor import JournalEditor
import popups


def _generate_label(parent_obj, size_qrect, label_name, text):
    label = QLabel(parent_obj)
    label.setGeometry(size_qrect)
    label.setObjectName(label_name)
    label.setText(text)
    return label


def _generate_line_edit(parent_obj, x, y, objname_prefix, text):
    le = QLineEdit(parent_obj)
    le.setGeometry(QRect(x, y, 290, 30))
    le.setObjectName(f"{objname_prefix}_line_edit")
    le.setMaxLength(160)
    le.setText(text)
    le.setEnabled(True)
    return le


class MyCharactersView(QWidget):
    def __init__(self, flags, *args, **kwargs):
        super().__init__(flags, *args, **kwargs)

        self.backend_communicator = backend_com.BackendCom.instance()

        self.outer_box = QGroupBox(self)
        self.outer_box.setObjectName("MyCharactersViewOuterBox")
        self.outer_box.setGeometry(QRect(0, 0, 960, 360))

        self.tab_widget = QTabWidget(self.outer_box)
        self.tab_widget.setObjectName("MyCharactersViewTabWidget")
        self.tab_widget.setGeometry(QRect(12, 10, 948, 350))
        self.tab_index_buffer = -1
        self.reload()
        self.tab_widget.currentChanged.connect(self._reset_delete_button)

    def _reset_delete_button(self, tab):
        if delete_character_button_widget := self.tab_widget.widget(tab):
            delete_character_button_widget.findChild(QPushButton, "delete_character_button").hide()
        if delete_character_pre_button_widget := self.tab_widget.widget(tab):
            delete_character_pre_button_widget.findChild(QPushButton, "delete_character_pre_button").show()

    def generate_character_tab(self, ci):
        STATUS_INFO_HELPER_TEXT = "This reflects the current state of your character. It could be things your" \
                                  " character currently does or a state, like \"sleeping\" or \"recovering from a" \
                                  " battle\"."

        ACTIVE_STATUS_INFO_HELPER_TEXT = "Setting a character as \"active\" makes it visible to other users. " \
                                         "Only one character can be active at the same time.\n" \
                                         "Setting a character active will deactivate all other characters.\n" \
                                         "If you have no active characters, you will not show up in the online list."
        tab = QWidget()
        tab_name = ci["forename"]
        line_edits = {}
        # header
        # title
        line_edits["title"] = QLineEdit(tab)
        line_edits["title"].setGeometry(QRect(10, 20, 100, 30))
        line_edits["title"].setMaxLength(15)
        line_edits["title"].setObjectName("title_line_edit")
        line_edits["title"].setText(ci["title"])
        line_edits["title"].setPlaceholderText("title")
        # forename
        line_edits["forename"] = QLineEdit(tab)
        line_edits["forename"].setGeometry(QRect(120, 20, 200, 30))
        line_edits["forename"].setMaxLength(50)
        line_edits["forename"].setObjectName("forename_line_edit")
        line_edits["forename"].setText(ci["forename"])
        line_edits["forename"].setPlaceholderText("forename")
        # middlename
        line_edits["middlename"] = QLineEdit(tab)
        line_edits["middlename"].setGeometry(QRect(330, 20, 200, 30))
        line_edits["middlename"].setMaxLength(50)
        line_edits["middlename"].setObjectName("middlename_line_edit")
        line_edits["middlename"].setText(ci["middlename"])
        line_edits["middlename"].setPlaceholderText("middlename")
        # surname
        line_edits["surname"] = QLineEdit(tab)
        line_edits["surname"].setGeometry(QRect(540, 20, 200, 30))
        line_edits["surname"].setMaxLength(50)
        line_edits["surname"].setObjectName("surname_line_edit")
        line_edits["surname"].setText(ci["surname"])
        line_edits["surname"].setPlaceholderText("surname")
        # journal button
        journal_editor_button = QPushButton(QIcon("./views/img/icon_journal.svg"), "", tab)
        journal_editor_button.setFlat(True)
        journal_editor_button.setIconSize(QSize(30, 30))
        journal_editor_button.setGeometry(QRect(910, 20, 30, 30))
        journal_editor_button.clicked.connect(lambda: self.open_journal_editor(ci["character_id"], ci["forename"]))
        # webview button
        line_edits["webview_setting"] = QLineEdit(tab)
        line_edits["webview_setting"].setGeometry(QRect(0, 0, 0, 0))
        line_edits["webview_setting"].setObjectName("webview_setting_line_edit")
        line_edits["webview_setting"].setText(str(ci["webview_setting"]))
        webview_settings_button = QPushButton(QIcon("./views/img/icon_webview.png"), "", tab)
        webview_settings_button.setFlat(True)
        webview_settings_button.setIconSize(QSize(30, 30))
        webview_settings_button.setGeometry(QRect(872, 20, 30, 30))
        webview_settings_button.clicked.connect(lambda: self.open_webview_settings(ci["character_id"],
                                                                                   line_edits["webview_setting"]))

        # appearance
        appearance_box = QGroupBox(tab)
        appearance_box.setObjectName("appearance_box")
        appearance_box.setGeometry(QRect(10, 55, 934, 220))
        appearance_box.setTitle("appearance")
        # race
        _generate_label(appearance_box, QRect(10, 25, 160, 30), "race_label", "race")
        line_edits["race"] = _generate_line_edit(appearance_box, 170, 25, "race", ci["race"])
        # eyes
        _generate_label(appearance_box, QRect(10, 55, 160, 30), "eyes_label", "eyes")
        line_edits["eyes"] = _generate_line_edit(appearance_box, 170, 55, "eyes", ci["eyes"])
        # hair/fur
        _generate_label(appearance_box, QRect(10, 85, 160, 30), "hair_fur_label", "hair / fur")
        line_edits["hair_fur"] = _generate_line_edit(appearance_box, 170, 85, "hair_fur", ci["hair_fur"])
        # age
        _generate_label(appearance_box, QRect(10, 115, 160, 30), "age_label", "age")
        line_edits["age"] = _generate_line_edit(appearance_box, 170, 115, "age", ci["age"])
        # body type
        _generate_label(appearance_box, QRect(10, 145, 160, 30), "body_type_label", "body type")
        line_edits["body_type"] = _generate_line_edit(appearance_box, 170, 145, "body_type", ci["body_type"])
        # voice
        _generate_label(appearance_box, QRect(480, 25, 160, 30), "voice_label", "voice")
        line_edits["voice"] = _generate_line_edit(appearance_box, 640, 25, "voice", ci["voice"])
        # accessories
        _generate_label(appearance_box, QRect(480, 55, 160, 30), "accessories_label", "accessories")
        line_edits["accessories"] = _generate_line_edit(appearance_box, 640, 55, "accessories", ci["accessories"])
        # notable characteristics
        _generate_label(appearance_box, QRect(480, 85, 160, 30),
                        "notable_characteristics_label", "notable characteristics")
        line_edits["notable_characteristics"] = _generate_line_edit(appearance_box, 640, 85, "notable_characteristics",
                                                                    ci["notable_characteristics"])
        # racial features
        _generate_label(appearance_box, QRect(480, 115, 160, 30),
                        "racial_features_label", "racial features")
        line_edits["racial_features"] = _generate_line_edit(appearance_box, 640, 115, "racial_features",
                                                            ci["racial_features"])
        # posture
        _generate_label(appearance_box, QRect(480, 145, 160, 30), "posture_label", "posture")
        line_edits["posture"] = _generate_line_edit(appearance_box, 640, 145, "posture", ci["posture"])

        # current status
        _generate_label(appearance_box, QRect(10, 175, 160, 30), "current_status_label", "current status")
        current_status_helper_button = QPushButton(QIcon("./views/img/question_mark_icon30x30.png"), "", appearance_box)
        current_status_helper_button.setGeometry(QRect(110, 175, 30, 30))
        current_status_helper_button.setFlat(True)
        current_status_helper_button.clicked.connect(lambda: popups.show_info_popup("character status",
                                                                                    STATUS_INFO_HELPER_TEXT))
        line_edits["current_status"] = _generate_line_edit(appearance_box, 170, 175, "current_status",
                                                           ci["current_status"])

        # active
        active_checkbox = QCheckBox("active character", tab)
        active_checkbox.setGeometry(QRect(180, 290, 160, 30))
        active_status_helper_button = QPushButton(QIcon("./views/img/question_mark_icon30x30.png"), "", tab)
        active_status_helper_button.setGeometry(QRect(280, 290, 30, 30))
        active_status_helper_button.setFlat(True)
        active_status_helper_button.clicked.connect(lambda: popups.show_info_popup("active status",
                                                                                   ACTIVE_STATUS_INFO_HELPER_TEXT))
        if ci["active"] == 1:
            active_checkbox.setChecked(True)
        else:
            active_checkbox.setChecked(False)
        active_checkbox.stateChanged.connect(lambda: update_button.setEnabled(True))

        # delete character, new character and update button
        delete_character_pre_button = QPushButton(tab)
        delete_character_pre_button.setObjectName("delete_character_pre_button")
        delete_character_pre_button.setGeometry(QRect(474, 290, 150, 30))
        delete_character_pre_button.setText("delete character")
        delete_character_pre_button.clicked.connect(
            lambda: delete_character_button.show() if delete_character_pre_button.hide() is None else None)
        delete_character_button = QPushButton(tab)
        delete_character_button.setObjectName("delete_character_button")
        delete_character_button.setGeometry(QRect(474, 290, 150, 30))
        delete_character_button.setText("are you sure?")
        delete_character_button.clicked.connect(
            lambda: self.reload() if self.backend_communicator.delete_character(ci["character_id"]) is not False
            else None)
        delete_character_button.hide()

        new_character_button = QPushButton(tab)
        new_character_button.setGeometry(QRect(630, 290, 150, 30))
        new_character_button.setText("add new character")
        new_character_button.clicked.connect(self.add_new_character)

        update_button = QPushButton(tab)
        update_button.setGeometry(QRect(786, 290, 150, 30))
        update_button.setText("update")
        update_button.setEnabled(False)
        update_button.clicked.connect(lambda: self._update(ci, line_edits, active_checkbox.isChecked(), update_button))
        # enable update button whenever a lineedit is modified by the user
        for line_edit in line_edits.values():
            line_edit.textChanged.connect(lambda: update_button.setEnabled(True))

        edit_bio_button = QPushButton(tab)
        edit_bio_button.setGeometry(QRect(318, 290, 150, 30))
        edit_bio_button.setText("edit biography")
        edit_bio_button.clicked.connect(lambda: self.open_bio_editor(ci["character_id"]))

        return tab, tab_name

    def _update(self, ci, line_edits, active, update_button):
        new_character_values = {k: line_edits[k].text() for k in line_edits.keys()}
        success = self.backend_communicator.update_character({"char_id": ci["character_id"]},
                                                             new_character_values, active)
        self.reload() if success else update_button.setEnabled(True)

    # opens dialogue box for adding a new character, and adds it
    def add_new_character(self):
        new_forename, ok = QInputDialog.getText(self, 'add new character', 'Enter new characters forename:')
        new_forename = str(new_forename)
        if ok and new_forename != "":
            if self.backend_communicator.add_character(new_forename):
                self.reload()
                return True
        return False

    # reloads all tabs with latest data
    def reload(self):
        self.tab_index_buffer = self.tab_widget.currentIndex()
        self.tab_widget.clear()
        my_characters = self.backend_communicator.get_data_from_endpoint("characters/getall")
        if not my_characters and type(my_characters) == bool:  # wrong server configuration or server down
            return
        for character in my_characters:
            tab, tab_name = self.generate_character_tab(character)
            self.tab_widget.addTab(tab, tab_name)
        if not self.tab_widget.count():
            self.tab_widget.addTab(self.emergency_tab(), "add a new character")
        if self.tab_widget.count() >= self.tab_index_buffer:
            self.tab_widget.setCurrentIndex(self.tab_index_buffer)
        else:
            self.tab_widget.setCurrentIndex(-1)

    def emergency_tab(self):
        emergency_tab = QWidget()
        emergency_button = QPushButton("It looks like you do not have any characters\n"
                                       "Click me if you want to create one.", emergency_tab)
        emergency_button.setGeometry(QRect(30, 30, 890, 270))
        emergency_button.clicked.connect(self.add_new_character)
        return emergency_tab

    def open_bio_editor(self, current_character_id):
        self.bio_editor = BiographyEditor(self, current_character_id)
        self.bio_editor.show()

    def open_journal_editor(self, current_character_id, character_name):
        self.journal_editor = JournalEditor(self, current_character_id, character_name)
        self.journal_editor.show()

    def open_webview_settings(self, character_id, line_edit_obj):
        def _change_webview_setting(enable: bool):
            line_edit_obj.setText("2" if enable else "0")
            webview_settings_box.setParent(None)

        webview_settings_box = QGroupBox(self)
        webview_settings_box.setGeometry(QRect(300, 100, 250, 150))
        webview_settings_box.setObjectName("WebviewSettingsMenu")
        webview_settings_box_header_label = QLabel("<center><h2>Webview</h2></center>", webview_settings_box)
        webview_settings_box_header_label.setGeometry(QRect(0, 10, 250, 20))
        webview_settings_selector_enabled = QRadioButton("enabled", webview_settings_box)
        webview_settings_selector_enabled.setGeometry(QRect(50, 50, 60, 15))
        webview_settings_selector_enabled.setChecked(line_edit_obj.text() == "2")

        webview_settings_selector_disabled = QRadioButton("disabled", webview_settings_box)
        webview_settings_selector_disabled.setGeometry(QRect(150, 50, 60, 15))
        webview_settings_selector_disabled.setChecked(line_edit_obj.text() == "0")
        webview_settings_box_info_label = QLabel("<center>Link to webview:</center>",
                                                 webview_settings_box)
        webview_settings_box_info_label.setGeometry(QRect(0, 67, 250, 15))
        webview_settings_link_box = QLineEdit(f"https://gw2rpc.net/character/{character_id}", webview_settings_box)
        webview_settings_link_box.setReadOnly(True)
        webview_settings_link_box.setAlignment(Qt.AlignCenter)
        webview_settings_link_box.setGeometry(QRect(10, 87, 230, 25))
        webview_settings_box_close_button = QPushButton("close", webview_settings_box)
        webview_settings_box_close_button.setGeometry(QRect(5, 120, 115, 25))
        webview_settings_box_close_button.clicked.connect(lambda: webview_settings_box.setParent(None))
        webview_settings_box_save_button = QPushButton("save", webview_settings_box)
        webview_settings_box_save_button.setGeometry(QRect(130, 120, 115, 25))
        webview_settings_box_save_button.clicked.connect(lambda:
                                                         _change_webview_setting(
                                                             webview_settings_selector_enabled.isChecked()))
        webview_settings_box.show()
